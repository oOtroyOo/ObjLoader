﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonTool
{
    public interface ISerializerInterface
    {
        T FromData<T>(string value);
        string ToDataString<T>(T obj);

        object FromData(string value, Type type);
        string ToDataString(object obj, Type type);
    }
}
