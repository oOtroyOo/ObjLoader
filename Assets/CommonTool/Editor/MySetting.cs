﻿#if UNITY_EDITOR
using UnityEditor;
#if UNITY_EDITOR||!(UNITY_WEBGL||UNITY_WEBPLAYER)
using System.IO;
#endif
#endif
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CommonTool
{
    public class MySetting : Editor
    {
        [InitializeOnLoadMethod]
        public static void OnEditorLoad()
        {
            Settings();
            ScriptTemplate();
        }

        static void Settings()
        {
            //Debug.Log("Settings");
            PlayerSettings.defaultIsFullScreen = false;
            PlayerSettings.defaultScreenWidth = 1600;
            PlayerSettings.defaultScreenHeight = 900;
            PlayerSettings.runInBackground = true;
            PlayerSettings.resizableWindow = true;
            PlayerSettings.displayResolutionDialog = ResolutionDialogSetting.Disabled;
            PlayerSettings.allowFullscreenSwitch = true;
            PlayerSettings.forceSingleInstance = true;

            PlayerSettings.defaultInterfaceOrientation = UIOrientation.AutoRotation;
            PlayerSettings.allowedAutorotateToPortrait = false;
            PlayerSettings.allowedAutorotateToPortraitUpsideDown = false;
            PlayerSettings.allowedAutorotateToLandscapeLeft = true;
            PlayerSettings.allowedAutorotateToLandscapeRight = true;
#if UNITY_5_6 || UNITY_5_6_OR_NEWER
#if UNITY_2017_OR_NEWER
            if (PlayerSettings.scriptingRuntimeVersion == ScriptingRuntimeVersion.Legacy)
#endif
            {
                PlayerSettings.SetApiCompatibilityLevel(EditorUserBuildSettings.selectedBuildTargetGroup, ApiCompatibilityLevel.NET_2_0);
            }
#else
            PlayerSettings.apiCompatibilityLevel = ApiCompatibilityLevel.NET_2_0;
#endif
            PlayerSettings.Android.preferredInstallLocation = AndroidPreferredInstallLocation.PreferExternal;
            PlayerSettings.Android.forceInternetPermission = true;
            PlayerSettings.Android.forceSDCardPermission = true;

            EditorSettings.serializationMode = SerializationMode.ForceText;


        }

        static void ScriptTemplate()
        {
            string nameunity = "#SCRIPTNAME#";
            string namecs = "$safeitemname$";
            //Debug.Log("StriptTemplate");
            for (int i = 0; i < 2; i++)
            {

                string name = i == 0 ? nameunity : namecs;
                string data = @"using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random=UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class " + name + @" : MonoBehaviour
{
    public virtual void Awake()
    {

    }

    public virtual void Start()
    {

    }

    public virtual void Update()
    {

    }

    public virtual void OnEnable()
    {

    }

    public virtual void OnDisable()
    {

    }

    public virtual void FixedUpdate()
    {

    }

    public virtual void LateUpdate()
    {

    }
}";
                Encoding encode = new System.Text.UTF8Encoding(true);
                byte[] bytes = encode.GetBytes(data);

                if (i == 0)
                {
                    FileInfo exe = new FileInfo(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
                    DirectoryInfo ScriptTemplates = new DirectoryInfo(Path.Combine(exe.Directory.FullName, @"Data\Resources\ScriptTemplates"));

                    foreach (FileInfo csfile in ScriptTemplates.GetFiles())
                    {
                        if (csfile.Name.Contains("NewBehaviourScript.cs"))
                        {
                            if (csfile.Length != bytes.Length)
                            {
                                File.WriteAllBytes(csfile.FullName, bytes);
                            }
                        }
                    }
                }
                else
                {
                    FileInfo csfile = null;
                    foreach (var devenv in System.Diagnostics.Process.GetProcessesByName("devenv"))
                    {
                        //D:\Program Files\Microsoft Visual Studio\2017\Common7\IDE\Extensions\Microsoft\Visual Studio Tools for Unity\ItemTemplates\CSharp\1033\CSharp MonoBehaviour\NewMonoBehaviour.cs
                        //D:\Program Files\Microsoft Visual Studio\2017\Common7\IDE\devenv.exe
                        FileInfo exe = new FileInfo(devenv.MainModule.FileName);
                        DirectoryInfo dir = new DirectoryInfo(Path.Combine(exe.Directory.FullName, @"Extensions\Microsoft\Visual Studio Tools for Unity\ItemTemplates\CSharp"));
                        if (dir.Exists)
                        {
                            foreach (DirectoryInfo directory in dir.GetDirectories())
                            {
                                string filename = Path.Combine(directory.FullName, @"CSharp MonoBehaviour\NewMonoBehaviour.cs");
                                if (File.Exists(filename))
                                {
                                    csfile = new FileInfo(filename);
                                    break;
                                }
                            }
                        }
                    }
                    if (csfile != null)
                    {
                        if (csfile.Length != bytes.Length)
                        {
                            File.WriteAllBytes(csfile.FullName, bytes);
                        }
                    }

                }
            }
        }
    }
}