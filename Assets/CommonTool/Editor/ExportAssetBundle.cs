﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class ExportAssetBundle : Editor
{

    [MenuItem("Assets/clean cache")]
    static void clean()
    {
        AssetDatabase.Refresh();
        var a = AssetDatabase.GetAllAssetPaths();
        for (int i = 0; i < a.Length; i++)
        {
            if (Directory.Exists(a[i]))
            {
                if (Directory.GetFiles(a[i]).Length == 0)
                {
                    AssetDatabase.DeleteAsset(a[i]);
                }
            }
        }
        Animator[] animators = GameObject.FindObjectsOfType<Animator>();
        foreach (Animator animator in animators)
        {
            if (animator.runtimeAnimatorController == null && animator.avatar == null)
            {
                DestroyImmediate(animator);
            }
        }

#if UNITY_2017_OR_NEWER || UNITY_2017

        Caching.ClearCache();
#else
        Caching.CleanCache();
#endif
        System.GC.Collect();

        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
    }

    private static AssetBundle maniBundle;
    [MenuItem("Assets/AssetBundle/Build all")]
    static void ExportBuildAll()
    {

        Debug.Log("Build all " + EditorUserBuildSettings.activeBuildTarget);
        if (!string.IsNullOrEmpty(EditorSceneManager.GetActiveScene().name) && EditorSceneManager.GetActiveScene().name != "Untitled")
        {
            EditorSceneManager.SaveOpenScenes();
        }
        AssetDatabase.SaveAssets();

        AssetBundleManifest oldManif = null;
        if (File.Exists(Application.streamingAssetsPath + "/StreamingAssets"))
        {
            if (maniBundle != null)
            {
                maniBundle.Unload(true);
                maniBundle = null;
            }
            maniBundle = AssetBundle.LoadFromFile(Application.streamingAssetsPath + "/StreamingAssets");
            if (maniBundle != null)
            {
                foreach (AssetBundleManifest loadAllAsset in maniBundle.LoadAllAssets<AssetBundleManifest>())
                {
                    oldManif = loadAllAsset;
                }
                maniBundle.Unload(false);
            }
        }
        if (!Directory.Exists(Application.streamingAssetsPath))
        {
            Directory.CreateDirectory(Application.streamingAssetsPath);
        }
        AssetBundleManifest assetBundleManifest = BuildPipeline.BuildAssetBundles(Application.streamingAssetsPath, BuildAssetBundleOptions.DeterministicAssetBundle, EditorUserBuildSettings.activeBuildTarget);

        var bundles = assetBundleManifest.GetAllAssetBundles();



        for (int i = 0; i < bundles.Length; i++)
        {
            Hash128 hash = assetBundleManifest.GetAssetBundleHash(bundles[i]);

            bool oldbundle = oldManif != null && oldManif.GetAllAssetBundles().Contains(bundles[i]);
            if (!oldbundle || !string.Equals(oldManif.GetAssetBundleHash(bundles[i]).ToString(), hash.ToString(), StringComparison.CurrentCultureIgnoreCase))
            {
                Debug.Log(bundles[i] + "\n" + hash.ToString());
            }
            using (StreamWriter writer = File.CreateText(Application.streamingAssetsPath + "/" + bundles[i] + ".hash"))
            {
                writer.WriteLine(hash.ToString());
                foreach (string dependency in assetBundleManifest.GetAllDependencies(bundles[i]))
                {
                    writer.WriteLine(dependency);
                }
            }
        }
        if (oldManif != null)
        {
            var todelete = oldManif.GetAllAssetBundles().Where(s => !bundles.Contains(s));
            foreach (string s in todelete)
            {
                if (File.Exists(Application.streamingAssetsPath + "/" + s))
                {
                    File.Delete(Application.streamingAssetsPath + "/" + s);
                }
                if (File.Exists(Application.streamingAssetsPath + "/" + s + ".manifest"))
                {
                    File.Delete(Application.streamingAssetsPath + "/" + s + ".manifest");
                }
                Debug.Log("delete " + s);
            }
        }
        foreach (FileInfo fileInfo in new DirectoryInfo(Application.streamingAssetsPath).GetFiles())
            //{
            //    if (fileInfo.Extension == ".manifest")
            //    {
            //        fileInfo.Delete();
            //    }
            //}
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        Debug.Log("Build Finish");
    }



}
