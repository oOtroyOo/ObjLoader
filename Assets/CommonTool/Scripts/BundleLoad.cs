﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if !UNITY_5_4_OR_NEWER
using UnityEngine.Experimental.Networking;
#else
using UnityEngine.Networking;
#endif
using Random = UnityEngine.Random;
namespace CommonTool
{

    /// <summary>
    /// assetbunle 加载类
    /// </summary>
    public class BundleLoad : IDisposable
    {
        private static bool anyLoading = false;
        private static Dictionary<string, AssetBundle> loadedBundle = new Dictionary<string, AssetBundle>();

        public event Action<float> onProcessChange;
        public int totalCount { get; private set; }



        /// <summary> 如果异步下载失败则尝试同步加载</summary>
        public bool tryAgain = false;

        /// <summary>AssetBundleManifest资源 </summary>
        private AssetBundleManifest _manifest;

        public string Bundlename { get; private set; }
        public AssetBundle Bundle
        {
            get { return loadedBundle.ContainsKey(Bundlename) ? loadedBundle[Bundlename] : null; }
        }

        /// <summary> 是否完成 </summary>
        public bool IsDone
        {
            get { return Math.Abs(Progress - 1) < 0.001f; }
        }
        /// <summary> 是否错误 </summary>
        public string Error { get; private set; }

        /// <summary>
        /// 下载进度 0~1
        /// </summary>
        public float Progress
        {
            get
            {
                return (_dependenciesBundle.Sum(d => d.Progress) + _currentProgress) / totalCount;
            }
        }



        /// <summary> 是否异步下载 </summary>
        private bool _asyn = true;
        /// <summary>当前www加载进度 </summary>
        private float _currentProgress;


        private List<BundleLoad> _dependenciesBundle = new List<BundleLoad>();

        /// <summary>
        /// 初始化加载，默认根路径为Loading.strStorage
        /// </summary>
        /// <param name="bundlename"></param>
        public BundleLoad(string bundlename, AssetBundleManifest manifest)
        {
            _manifest = manifest;
            this.Bundlename = bundlename;
            if (manifest != null)
            {
                foreach (string name in manifest.GetAllAssetBundles())
                {
                    if (string.Equals(name, bundlename, StringComparison.CurrentCultureIgnoreCase))
                    {
                        this.Bundlename = name;
                    }
                }
            }
            //ApplicationHandler.GetInstance().onApplcationQuit.AddListener(Dispose);
        }


        /// <summary>
        /// 开启线程 是否异步下载
        /// </summary>
        /// <param name="bAsyn">是否异步下载</param>
        public Coroutine Start(bool bAsyn = true)
        {
            this._asyn = bAsyn;
            return Loading.Instance.StartCoroutine(Load());
        }

        /// <summary>
        /// 加载线程
        /// </summary>
        /// <returns></returns>
        private IEnumerator Load()
        {
            totalCount = 1;
            Error = "";
            _currentProgress = 0;
            OnProcessChange();
            yield return new WaitForFixedUpdate();


            //寻找包含关联的assetbundle 先加载。
            if (_manifest != null)
            {
                List<string> dbundleNames = GetDependencies();
                if (dbundleNames.Count > 0)
                {
                    Debug.Log(string.Format("{0}有{1}个关联包\n{2}", Bundlename, dbundleNames.Count, string.Join(",", dbundleNames.ToArray())));
                    yield return new WaitForFixedUpdate();
                    totalCount = dbundleNames.Count + 1;
                    foreach (string dbundleName in dbundleNames)
                    {
                        var dbundle = new BundleLoad(dbundleName, null);
                        _dependenciesBundle.Add(dbundle);
                        dbundle.onProcessChange += delegate
                        {
                            OnProcessChange();
                        };
                        yield return dbundle.Start(_asyn);
                    }
                }
            }

            Debug.Log((_asyn ? "异步" : "同步") + "下载bundle:" + Bundlename);
            //在已加载的assetBundle中搜索是否已加载本包名
            if (Bundle == null && !loadedBundle.ContainsKey(Bundlename))
            {
                while (anyLoading)
                {
                    yield return new WaitForSeconds(1);
                }
                loadedBundle.Add(Bundlename, null);
                anyLoading = true;
                string url = Loading.strStorage + Bundlename;

                //即将加载的bundle
                AssetBundle bundle = null;
                if (_asyn)
                {

                    //www下载
                    if (!url.StartsWith("jar") && !url.StartsWith("file") && !url.StartsWith("http") && !url.StartsWith("ftp"))
                    {
                        url = "file://" + url;
                    }
                    Debug.Log("url=" + url);

                    //WebRequest的下载
                    UnityWebRequest req = _manifest == null ?
                        UnityWebRequest.GetAssetBundle(url) :
#if UNITY_2017_OR_NEWER
                        UnityWebRequest.GetAssetBundle(url, new CachedAssetBundle(Bundlename, _manifest.GetAssetBundleHash(Bundlename)), 0);
#else
                        UnityWebRequest.GetAssetBundle(url,  _manifest.GetAssetBundleHash(Bundlename),0);
#endif
                    req.disposeDownloadHandlerOnDispose = true;
#if UNITY_2017_2_OR_NEWER
                    var asyn = req.SendWebRequest();
#else
                    var asyn = req.Send();
#endif
                    yield return asyn;
#if UNITY_2017_OR_NEWER
                    if (req.isNetworkError || req.isHttpError)
#else
                    if (req.isNetworkError)
#endif
                    {
                        Error = (req.error);
                    }
                    else
                    {
                        bundle = DownloadHandlerAssetBundle.GetContent(req);
                    }
                    /*

                    //www的下载
                    using (WWW www = (_manifest == null ? new WWW(url) : WWW.LoadFromCacheOrDownload(url, _manifest.GetAssetBundleHash(Bundlename))))
                    {
                        //等待www
                        while (!www.isDone)
                        {
                            _currentProgress = www.progress;
                            if (!string.IsNullOrEmpty(www.error))
                            {
                                Error = www.error;
                                break;
                            }

                            OnProcessChange();
                            yield return null;
                        }
                        if (string.IsNullOrEmpty(www.error))
                        {
                            bundle = www.assetBundle;
                        }
                    }
                    */

                }
                else
                {
                    //文件加载
                    bundle = AssetBundle.LoadFromFile(url);
                }
                if (bundle != null)
                {
                    bundle.name = Bundlename;
                    if (loadedBundle.ContainsKey(Bundlename))
                    {
                        loadedBundle[Bundlename] = bundle;
                    }
                    else
                    {
                        loadedBundle.Add(Bundlename, bundle);
                    }
                    Loading.Instance.StartCoroutine(RemoveNull());
                    Debug.Log("加载bundle:" + bundle.name + "完成");
                }
                else
                {
                    Error += url + " 下的assetBundle = null";
                }
                anyLoading = false;

                if (!string.IsNullOrEmpty(Error))
                {
                    _currentProgress = 0;
                    Debug.LogError(Error);
                    if (tryAgain && _asyn)
                    {
                        Debug.LogError(Error + "\n异步下载失败，尝试同步加载");
                        _asyn = false;
                        tryAgain = false;
                        Dispose();
                        yield return Load();
                    }
                }
                else
                {
                    _currentProgress = 1;
                }

                //销毁不用的东西
                //loadedBundle.RemoveAll(b => b == null);
            }
            OnProcessChange();
        }

        private void OnProcessChange()
        {
            if (onProcessChange != null)
            {
                onProcessChange.Invoke(Progress);
            }
        }

        private List<string> GetDependencies()
        {
            List<string> dependencies = new List<string>();
            if (_manifest != null)
            {
                foreach (string dependenbundle in _manifest.GetAllDependencies(Bundlename))
                {
                    dependencies.Add(dependenbundle);
                    var dbundle = new BundleLoad(dependenbundle, _manifest);
                    dbundle._manifest = _manifest;
                    dependencies.AddRange(dbundle.GetDependencies());
                    dbundle.Dispose();
                    dbundle = null;
                }
            }
            return dependencies.Distinct().ToList();
        }
        private IEnumerator RemoveNull()
        {
            while (Bundle != null)
            {
                yield return new WaitForSeconds(2);
            }
            Dispose();
        }

        public void Dispose()
        {
            if (loadedBundle.ContainsKey(Bundlename))
            {
                var b = loadedBundle[Bundlename];
                if (b != null)
                {
                    b.Unload(false);
                }
                loadedBundle.Remove(Bundlename);
            }
        }
    }

}
