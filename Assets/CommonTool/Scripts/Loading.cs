﻿
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR

#endif
/// <summary>
/// 加载相关类 ，是个MonoBehaviour
/// </summary>
namespace CommonTool
{


    public class Loading : MonoBehaviour
    {

        /// <summary>  资源存储根目录，末尾需加 '/'，默认为streamingAssetsPath </summary>
        public static string strStorage = /*Application.streamingAssetsPath + */"/";

        /// <summary> AssetBundleManifest 资源 </summary>
        public static AssetBundleManifest AssetManifest;
        public static bool bLoading;
        /// <summary> AssetBundleManifest 文件名 默认StreamingAssets</summary>
        public string manifestName = "StreamingAssets";
        /// <summary>默认UI资源名 </summary>
        public static string UIprefab = "LoadingCanvas";

        public GameObject loadingCanvas = null;

        //加载进度事件

        public event Action<string, float> onLoadingEvent;
        //加载完成事件
        public event Action onLoadingDone;



        private static float curProgress = 0;
        private static string currentSceneName;


        //异步对象
        private AsyncOperation async;
        private Coroutine _manifestCoroutine;
        private AssetBundle scenebundle = null;




        private static Loading _instance;
        public static Loading Instance
        {
            get
            {
                if (_instance == null)
                {
                    Initialize();
                }
                return _instance;
            }

        }
        static void Initialize()
        {
            if (_instance == null)
            {

                if (!Application.isPlaying)
                    return;

                var g = new GameObject("Loading");
                DontDestroyOnLoad(g);
                _instance = g.AddComponent<Loading>();

            }

        }


        public static float CurProgress
        {
            get
            {
                return curProgress;
            }

            set
            {
                if (value > 1f)
                {
                    Debug.LogError("curProgress " + value + " >1");
                }
                curProgress = value;
                if (Mathf.Abs(1 - curProgress) < 0.1f)
                {
                    //Debug.Log("curProgress" + ":" + curProgress);
                }
                else
                {
                    if (Instance.loadingCanvas == null && GameObject.Find(UIprefab) == null)
                    {
                        try
                        {
                            Instance.loadingCanvas = Instantiate(Resources.Load<GameObject>(UIprefab));
                            Instance.loadingCanvas.name = UIprefab;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            //throw;
                        }
                    }
                }
                if (Instance.onLoadingEvent != null)
                {
                    Instance.onLoadingEvent.Invoke(currentSceneName, curProgress);
                }
            }
        }

        void Awake()
        {
            _instance = this;
            strStorage = Application.streamingAssetsPath + "/";
        }

        void Start()
        {
            
        }

        private void HideLoading()
        {
            if (loadingCanvas != null)
            {
                loadingCanvas.SetActive(false);
            }
            CallerBase.GetInstance().HideLoading();
        }

        public bool LoadLevel(string SceneName)
        {
            Application.backgroundLoadingPriority = ThreadPriority.Low;



            StartCoroutine(LoadScene(SceneName));

            return true;

        }

        /// <summary>
        /// 异步读取unity3D文件并 载入场景
        /// </summary>
        /// <returns></returns> 
        IEnumerator LoadScene(string sceneName)
        {
            
            CurProgress = 0;
            bLoading = true;
            Debug.Log("准备场景" + sceneName);
            currentSceneName = sceneName;
            yield return new WaitForSeconds(0.2f);


            //预下载所有资源，并构建bundle的表

            if (Loading.AssetManifest == null)
            {
                yield return Loading.Instance.LoadManifest(manifestName);
            }

            //准备加载场景
            if (scenebundle != null)
            {
                scenebundle.Unload(true);
            }

            async = SceneManager.LoadSceneAsync(sceneName);
            if (async == null)
            {
                string bundleName = "";

                foreach (string bundle in Loading.AssetManifest.GetAllAssetBundles())
                {
                    if (string.Equals(bundle.Split('.')[0], sceneName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        bundleName = bundle;
                    }
                }
                if (string.IsNullOrEmpty(bundleName))
                {
                    Debug.LogError("场景资源未找到  " + sceneName);
                }
                else
                {

                    Debug.Log("准备读取场景资源：" + bundleName);



                    //下载场景资源
                    BundleLoad sceneBundleLoad = new BundleLoad(bundleName, AssetManifest);
                    sceneBundleLoad.onProcessChange += process =>
                    {
                        //显示下载进度
                        CurProgress = sceneBundleLoad.Progress * 0.9f;
                    };
                    yield return sceneBundleLoad.Start();

                    scenebundle = sceneBundleLoad.Bundle;
                }
                yield return null;
                yield return null;
                yield return null;
            }

            //加载场景
            Debug.Log("开始加载场景" + sceneName);

            if (async == null)
            {
                async = SceneManager.LoadSceneAsync(sceneName);
            }

            //  async.priority = 1;// ThreadPriority.Low;
            async.allowSceneActivation = false;
            while (async.progress < 0.9f)
            {
                Debug.Log("async.progress:" + async.progress);
                CurProgress = 0.9f + async.progress * 0.1f;
                yield return null;
            }
            Debug.Log("async.progress:" + async.progress);
            Debug.Log("场景资源载入完成");

            yield return new WaitForSeconds(1f);
            if (scenebundle != null)
            {
                scenebundle.Unload(false);
            }
            async.allowSceneActivation = true;

            while (SceneManager.GetActiveScene().name != sceneName)
            {
                yield return null;
            }
            if (onLoadingDone != null)
            {
                onLoadingDone.Invoke();
            }
            CurProgress = 1;
            bLoading = false;
            //读取完毕后返回， 系统会自动进入场景  
        }

        /// <summary>
        /// 加载并构建assetBundlemainAsset的表，错误则初始化为new
        /// </summary>
        /// <returns></returns>
        public Coroutine LoadManifest(string manifestName)
        {
            if (_manifestCoroutine == null)
            {
                _manifestCoroutine = StartCoroutine(loadmanifest(manifestName));
            }
            return _manifestCoroutine;

        }

        private IEnumerator loadmanifest(string manifestName)
        {
            string url = strStorage + manifestName;

            AssetBundle bundle = null;


            for (int i = 0; i < 2; i++)
            {
                bool asyn = i == 1;

                Debug.Log("开始" + (i == 0 ? "同步" : "异步") + "加载AssetBundleManifest");
                BundleLoad bundleLoad = new BundleLoad(manifestName, null);

                yield return bundleLoad.Start(asyn);
                if (bundleLoad.Bundle != null)
                {
                    bundle = bundleLoad.Bundle;
                    break;
                }
                else
                {
                    bundleLoad.Dispose();
                }

            }

            if (bundle != null)
            {
                foreach (AssetBundleManifest assetBundleManifest in bundle.LoadAllAssets<AssetBundleManifest>())
                {
                    Loading.AssetManifest = assetBundleManifest;
                    Debug.Log("加载AssetBundleManifest完成");
                }
            }
            else
            {
                Debug.LogError("加载AssetBundleManifest失败");
            }
        }






    }
}
