﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml;
using UnityEngine;
using UnityEngine.Events;

namespace CommonTool
{



    /// <summary>
    /// Transform助手
    /// </summary>
    public static class TransformHelper
    {
        /// <summary>
        /// 递归查找子物体
        /// </summary>
        /// <param name="trans">父物体</param>
        /// <param name="goName">查找的子物体名字</param>
        public static Transform FindChild(Transform trans, string goName)
        {
            //先在直接下级找
            Transform child = trans.Find(goName);
            //如果找到返回
            if (child != null) return child;
            //没找到，继续在每一个子物体的下级找
            Transform go = null;
            for (int i = 0; i < trans.childCount; i++)
            {
                child = trans.GetChild(i);
                go = FindChild(child, goName);
                if (go != null) return go;
            }
            return null;
        }

        #region MyRegion 查找复合条件的所有子物体

        private static List<Transform> list_TransformAllChild = new List<Transform>();

        /// <summary>
        /// 查找复合条件的所有子物体
        /// </summary>
        /// <param name="trans"></param>
        /// <returns></returns>
        public static List<Transform> AllChild(Transform trans, Func<Transform, bool> deligate)
        {
            list_TransformAllChild.Clear();
            recurrence(trans, list_TransformAllChild, deligate);
            return list_TransformAllChild;
        }

        private static void recurrence(Transform trans, List<Transform> tempList, Func<Transform, bool> deligate)
        {
            for (int i = 0; i < trans.childCount; i++)
            {
                var item = trans.GetChild(i);
                if (deligate(item))
                {
                    tempList.Add(item);
                }
                recurrence(item, tempList, deligate);
            }
        }

        #endregion MyRegion 查找复合条件的所有子物体

        /// <summary>
        /// 转向
        /// </summary>
        /// <param name="targetDirection">目标方向</param>
        /// <param name="transform">要转向的物体</param>
        /// <param name="rotationSpeed">转向速度</param>
        public static void LookAtTarget(Vector3 targetDirection, Transform transform, float rotationSpeed)
        {
            if (targetDirection != Vector3.zero)
            {
                var targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
                transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeed);
            }
        }

        /// <summary>
        /// 球形范围查找某个方向上符合要求的物体数组按与中心点与距离升序排列
        /// </summary>
        /// <param name="centure">中心点</param>
        ///  <param name="dirction">方向</param>
        /// <param name="radius">半径</param>
        /// <param name="halfAngle">夹角的一半</param>
        /// <param name="hight">高度</param>
        /// <param name="layerMask">层</param>
        /// <returns></returns>
        public static Collider[] AroundFind(Vector3 centure, Vector3 dirction, float radius, float halfAngle, float hight, LayerMask layerMask)
        {
            dirction = Vector3.ProjectOnPlane(dirction, Vector3.up);
            Collider[] hitInfo = Physics.OverlapSphere(centure, radius, layerMask);
            if (hitInfo.Length == 0) return null;
            Collider[] colliders = CollectionHelper.FindAll(hitInfo,
                p =>
                {
                    //角度
                    Vector3 tempPos = p.transform.position;
                    tempPos.y = centure.y;
                    float LAngle = Vector3.Angle(tempPos - centure, dirction);
                    //高度
                    float LHight = Mathf.Abs(p.transform.position.y - centure.y);

                    return LAngle < halfAngle && LHight < hight;
                }
                );
            if (colliders == null) return null;
            CollectionHelper.OrderBy(colliders,
               p =>
               {
                   return (p.transform.position - centure).sqrMagnitude;
               }

               );
            return colliders;
        }
        /// <summary>  
        /// 获取子对象变换集合  
        /// </summary>  
        /// <param name="obj"></param>  
        /// <returns></returns>  
        public static List<Transform> GetChildCollection(this Transform obj)
        {
            List<Transform> list = new List<Transform>();
            for (int i = 0; i < obj.childCount; i++)
            {
                list.Add(obj.GetChild(i));
            }
            return list;
        }

        /// <summary>  
        /// 获取子对象集合  
        /// </summary>  
        /// <param name="obj"></param>  
        /// <returns></returns>  
        public static List<GameObject> GetChildCollection(this GameObject obj)
        {
            var list = obj.transform.GetChildCollection();
            return list.ConvertAll(T => T.gameObject);
        }

        public static Transform GetRootParent(this Transform obj)
        {
            Transform Root = obj;
            while (Root.parent != null)
            {
                //Root = Root.root;   //transform.root,方法可以直接获取最上父节点。  
                Root = Root.parent;
            }
            return Root;
        }

        /// <summary>  
        /// 把源对象身上的所有组件，添加到目标对象身上  
        /// </summary>  
        /// <param name="origin">源对象</param>  
        /// <param name="target">目标对象</param>  
        public static void CopyComponent(GameObject origin, GameObject target)
        {
            var originComs = origin.GetComponents<Component>();
            foreach (var item in originComs)
            {
                target.AddComponent(item.GetType());
            }
        }

        /// <summary>  
        /// 改变游戏脚本  
        /// </summary>  
        /// <param name="origin"></param>  
        /// <param name="target"></param>  
        public static void ChangeScriptTo(this MonoBehaviour origin, MonoBehaviour target)
        {
            target.enabled = true;
            origin.enabled = false;
        }


        /// <summary>  
        /// 从当前对象的子对象中查找，返回一个用tag做标识的活动的游戏物体的链表.如果没有找到则为空.   
        /// </summary>  
        /// <param name="obj">对象Transform</param>  
        /// <param name="tag">标签</param>  
        /// <param name="transList">结果Transform集合</param> // 对一个父对象进行递归遍历，如果有子对象的tag和给定tag相符合时，则把该子对象存到 链表数组中  
        public static void FindGameObjectsWithTagRecursive(this Transform obj, string tag, ref List<Transform> transList)
        {
            foreach (var item in obj.transform.GetChildCollection())
            {
                // 如果子对象还有子对象，则再对子对象的子对象进行递归遍历  
                if (item.childCount > 0)
                {
                    item.FindGameObjectsWithTagRecursive(tag, ref transList);
                }

                if (item.tag == tag)
                {
                    transList.Add(item);
                }
            }
        }

        public static List<Transform> FindRecursive(this Transform obj, string name, bool ignoreCase = false)
        {
            return obj.FindRecursive<Transform>(name, ignoreCase);
        }
        public static List<Transform> FindRecursive(this Transform obj, Predicate<Transform> match)
        {
            List<Transform> find = new List<Transform>();
            if (match(obj))
            {
                find.Add(obj);
            }
            find.AddRange(obj.FindRecursive(match));
            return find;
        }
        public static List<T> FindRecursive<T>(this Transform obj, string name, bool ignoreCase) where T : Component
        {
            List<T> transList = new List<T>();
            foreach (var item in obj.transform.GetChildCollection())
            {
                // 如果子对象还有子对象，则再对子对象的子对象进行递归遍历  
                if (item.childCount > 0)
                {
                    transList.AddRange(item.FindRecursive<T>(name, ignoreCase));
                }

                if ((item is T || item.GetComponents<T>().Length > 0) && string.Equals(item.gameObject.name, name, ignoreCase ? StringComparison.CurrentCultureIgnoreCase : StringComparison.Ordinal))
                {
                    transList.Add(item.GetComponent<T>());
                }
            }
            return transList;
        }
        public static void FindGameObjectsWithTagRecursive(this GameObject obj, string tag, ref List<GameObject> objList)
        {
            List<Transform> list = new List<Transform>();
            obj.transform.FindGameObjectsWithTagRecursive(tag, ref list);

            objList.AddRange(list.ConvertAll(T => T.gameObject));
        }

        /// <summary>  
        /// 从父对象中查找组件  
        /// </summary>  
        /// <typeparam name="T">组件类型</typeparam>  
        /// <param name="com">物体组件</param>  
        /// <param name="parentLevel">向上查找的级别，使用 1 表示与本对象最近的一个级别</param>  
        /// <param name="searchDepth">查找深度</param>  
        /// <returns>查找成功返回相应组件对象，否则返回null</returns>  
        public static T GetComponentInParent<T>(this Component com, int parentLevel = 1, int searchDepth = int.MaxValue) where T : Component
        {
            searchDepth--;

            if (com != null && searchDepth > 0)
            {
                if (com.transform.parent != null)
                {
                    var component = com.transform.parent.GetComponent<T>();
                    if (component != null)
                    {
                        parentLevel--;
                        if (parentLevel == 0)
                        {
                            return component;
                        }
                    }
                }

                return com.transform.parent.GetComponentInParent<T>(parentLevel, searchDepth);
            }

            return null;
        }



        public static void SendMessage<T>(this GameObject go, UnityAction<T> action)
        {
            var comp = go.GetComponents<T>();
            if (comp == null || comp.Length == 0)
            {
                Debug.LogError(go.name + " does not have component of " + typeof(T).Name);
            }
            else
            {
                foreach (var c1 in comp)
                {
                    action.Invoke(c1);
                }
            }
        }
    }


    public static class ExtentionMethod
    {


        public static string GetValue(this XmlNode xmlElement, string keys)
        {
            XmlNodeList list = xmlElement.SelectNodes("key");
            if (list != null)
            {
                foreach (XmlElement xmlElemetn in list)
                {
                    if (xmlElemetn.HasAttributes)
                    {
                        string keystr = xmlElemetn.GetAttribute("name");
                        if (!string.IsNullOrEmpty(keystr) && string.Equals(keystr, keys, StringComparison.CurrentCultureIgnoreCase))
                        {
                            return xmlElemetn.GetAttribute("value");
                        }
                    }
                }
            }
            Debug.Log("没有找到xml:" + xmlElement.Name + "对应值:" + keys);
            return string.Empty;
        }

        public static Dictionary<string, string> GetValues(this XmlNode xmlElement, string elementname = "key")
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            XmlNodeList list = xmlElement.SelectNodes(elementname);
            if (list != null)
            {
                foreach (XmlElement xmlElemetn in list)
                {
                    if (xmlElemetn.HasAttributes)
                    {
                        string keystr = xmlElemetn.GetAttribute("name");
                        string value = xmlElemetn.GetAttribute("value");
                        dic.Add(keystr, value);

                    }
                }
            }
            return dic;
        }

        public static FileInfo[] FindFiles(this DirectoryInfo directory, string key, string[] ex = null)
        {
            List<FileInfo> files = new List<FileInfo>();
            foreach (FileInfo fileInfo in directory.GetFiles())
            {
                if (!fileInfo.Name.Contains(key))
                {
                    continue;
                }
                bool fall = false;
                if (ex != null)
                {
                    foreach (string s in ex)
                    {
                        fall |= fileInfo.Name.IndexOf(s, StringComparison.CurrentCultureIgnoreCase) >= 0;
                    }
                }
                if (!fall)
                {
                    files.Add(fileInfo);
                }
            }
            foreach (DirectoryInfo direct in directory.GetDirectories())
            {
                bool fall = false;
                if (ex != null)
                {
                    foreach (string s in ex)
                    {
                        fall |= direct.Name.IndexOf(s, StringComparison.CurrentCultureIgnoreCase) >= 0;

                    }
                }
                if (!fall)
                {

                    files.AddRange(direct.FindFiles(key, ex));
                }
            }
            return files.ToArray();
        }

        public static string ToLongString(this Vector3 vector)
        {
            return "(" + vector.x + "," + vector.y + "," + vector.z + ")";
        }
    }

  
}