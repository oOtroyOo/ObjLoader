using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System;

namespace CommonTool
{
    /// <summary>
    /// XML序列化
    /// </summary>
    public class XMLUtils
    {
        /// <summary>
        /// 字节转string
        /// </summary>
        /// <param name="b">字节数组</param>
        /// <returns></returns>
        public static string UTF8ByteArrayToString(byte[] b)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            string s = encoding.GetString(b);
            return (s);
        }

        /// <summary>
        /// 字符串转字节数组
        /// </summary>
        /// <param name="s">字符内容</param>
        /// <returns></returns>
        public static byte[] StringToUTF8ByteArray(string s)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] b = encoding.GetBytes(s);
            return b;
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <typeparam name="T">类型</param>
        /// <param name="xmlString">string内容</param>
        /// <returns></returns>
        public static T DeserializeXML_Str<T>(string xmlString) where T : class
        {
            return (T)DeserializeObject(xmlString, typeof(T));
        }

        /// <summary>
        /// 反序列化文件
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="xmlPath">文件路径</param>
        /// <returns></returns>
        public static T DeserializeXML<T>(string xmlPath) where T : class
        {
            using (XmlReader xr = XmlReader.Create(xmlPath))
            {
                XmlSerializer xs = new XmlSerializer(typeof(T));
                return xs.Deserialize(xr) as T;
            }
        }

        /// <summary>
        /// 序列化--XML文件
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="mObject">对象</param>
        /// <param name="XMLPath">生成的xml路径</param>
        public static void SerializeXML<T>(object mObject, string XMLPath)
        {

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "    ";
            settings.NewLineChars = "\r\n";
            settings.Encoding = Encoding.UTF8;
            settings.OmitXmlDeclaration = true;
            using (XmlWriter xw = XmlWriter.Create(XMLPath, settings))
            {
                XmlSerializer xs = new XmlSerializer(typeof(T));
                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                namespaces.Add(string.Empty, string.Empty);
                xs.Serialize(xw, mObject, namespaces);
            }
        }

        public static string SerializeObject<T>(T pObject)
        {
            return SerializeObject(pObject, typeof(T));
        }

        public static string SerializeObject(object pObject, System.Type ty)
        {
            string data = UTF8ByteArrayToString(SerializeObjectByte(pObject, ty));
            return data;
        }

        public static byte[] SerializeObjectByte(object pObject, System.Type ty)
        {

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            //settings.IndentChars = "    ";
            //settings.NewLineChars = "\r\n";
            settings.Encoding = new UTF8Encoding(false);
            settings.OmitXmlDeclaration = true;

            using (MemoryStream stream = new MemoryStream())
            {
                using (XmlWriter xw = XmlWriter.Create(stream, settings))
                {
                    XmlSerializer xs = new XmlSerializer(ty);
                    XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                    namespaces.Add(string.Empty, string.Empty);
                    xs.Serialize(xw, pObject, namespaces);
                    return stream.ToArray();
                }
            }
        }

        public static T DeserializeObject<T>(string pXmlizedString)
        {
            return (T)DeserializeObject(pXmlizedString, typeof(T));
        }

        public static object DeserializeObject(string pXmlizedString, System.Type ty)
        {
            XmlSerializer xs = new XmlSerializer(ty);
            MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(pXmlizedString));
            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
            return xs.Deserialize(memoryStream);
        }

        public static T DeserializeObjectByte<T>(byte[] bytes)
        {
            XmlSerializer xs = new XmlSerializer(typeof(T));
            MemoryStream memoryStream = new MemoryStream(bytes);
            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
            return (T)xs.Deserialize(memoryStream);
        }

    }

}