﻿using UnityEngine;
using UnityEngine.Events;

namespace CommonTool
{

    public class ApplicationHandler : MonoBehaviour
    {
        public UnityEvent onApplcationQuit= new UnityEvent();
        public UnityEvent<bool> onApplicationPause;
        public UnityEvent<bool> onApplicationFocus;
        private static ApplicationHandler _instance;

        public static ApplicationHandler GetInstance()
        {
            if (_instance == null)
            {
                GameObject GO = new GameObject("ApplicationHandler");
                _instance = GO.AddComponent<ApplicationHandler>();
                DontDestroyOnLoad(GO);
            }
            return _instance;
        }

        private void Awake()
        {
            if (_instance != null)
            {
                Destroy(_instance);
                Destroy(_instance.gameObject);
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }


        [RuntimeInitializeOnLoadMethod]
        public static void RuntimeInitialize()
        {
            GetInstance();

        }

        private void OnApplicationFocus(bool focus)
        {
            Debug.Log("OnApplicationFocus "+focus);
            if (onApplicationFocus != null) onApplicationFocus.Invoke(focus);
        }

        private void OnApplicationPause(bool pause)
        {
            Debug.Log("OnApplicationFocus " + pause);
            if (onApplicationPause != null) onApplicationPause.Invoke(pause);
        }

        private void OnApplicationQuit()
        {
            Debug.Log("OnApplicationQuit");
            if (onApplcationQuit != null) onApplcationQuit.Invoke();
        }


    }


}