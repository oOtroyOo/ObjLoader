﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

using UnityEngine;
#if !UNITY_5_4_OR_NEWER
using UnityEngine.Experimental.Networking;
#else
using UnityEngine.Networking;
#endif
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
#if UNITY_EDITOR||!(UNITY_WEBGL||UNITY_WEBPLAYER)
using System.IO;
#endif
#endif

namespace CommonTool
{
    public class CallerBase : InstanceMonoBehavior<CallerBase>
    {

        private static DateTime _date;
        public static DateTime Date
        {
            get
            {
                return _date.Add(_stopwatch.Elapsed);
            }
            set
            {
                _date = value;
                _stopwatch = new Stopwatch();
                _stopwatch.Reset();
                _stopwatch.Start();
            }
        }
        private static Stopwatch _stopwatch;
        public delegate void CallBack(string response);

        public class Setting
        {
            public Dictionary<string, string> headers = null;
            public bool alwaysLoadFile = false;
        }


        public virtual string hostUrl
        {
            get { return "http://localhost"; }
        }
        private Dictionary<int, string> responseDictionary = new Dictionary<int, string>();
        protected Dictionary<int, Setting> settingDictionary = new Dictionary<int, Setting>();

        /// <summary>
        /// 回调结果
        /// </summary>
        /// <param name="value"></param>
        public void Response(string value)
        {
            string data = value;
            //提取前几个hash字符
            if (value.Length > 8)
            {
                string sub = value.Substring(0, 8);

                int hash = 0;
                if (Int32.TryParse(sub, NumberStyles.HexNumber, null, out hash))
                {

                    data = value.Substring(8, value.Length - 8);
                    Debug.Log("接收到回调(" + sub + ")\n" + data);
                    responseDictionary.AddValue(hash, data);
                    return;

                }
            }
            Debug.LogError("回调未包含HASH开头\n" + value);
        }

        /// <summary>
        /// <para>发送请求，请求格式参考</para> 
        /// <para>loginAjax?username=nine&password=admin&hash=(32位数)</para>
        /// <para>RelativeUrl 网页上请求相对地址</para>
        /// <para>param 请求参数，参考 username=nine&password=admin&hash=(32位数)</para>
        /// <para>nmethod Unity回调方法，参数为网页返回string值</para> 
        /// </summary>
        /// <param name="RelativeUrl">网页上请求相对地址</param>
        /// <param name="param">请求参数，参考 username=nine&password=admin&hash=(32位数)</param>
        /// <param name="method">Unity回调方法，参数为网页返回string值</param>
        /// <param name="headers">请求Header</param>
        /// <returns></returns>
        protected IEnumerator SendHttpGetRequest(string RelativeUrl, Dictionary<string, string> param, CallBack method, Setting callSetting = null)
        {

            //生成一个唯一hash
            int hash = (Random.Range(0, 5f)).GetHashCode();
            settingDictionary.Add(hash, callSetting);
            //login特殊处理，如果未登录则先请求login
            if (!isLogin() && RelativeUrl.IndexOf("login", StringComparison.CurrentCultureIgnoreCase) < 0)
            {
                yield return Login();
            }

            //参数格式
            string getparam;
            //组合网页相对参数格式
            var requesturl = GetRequestUrl(hash, RelativeUrl, out getparam, ref param);
            Debug.Log("Get请求：" + requesturl);


            #region UnityWebRequest

            var req = UnityWebRequest.Get(hostUrl + "/" + requesturl);
            if (callSetting != null)
            {
                if (callSetting.headers != null)
                {
                    foreach (var header in callSetting.headers)
                    {
                        req.SetRequestHeader(header.Key, header.Value);
                    }
                }
            }
            req.disposeDownloadHandlerOnDispose = true;
#if UNITY_5_6_OR_NEWER
            req.timeout = 1;
#endif

#if UNITY_2017_2_OR_NEWER
            var asyn = req.SendWebRequest();
#else
            var asyn = req.Send();
#endif
            Action<UnityWebRequest> action = delegate (UnityWebRequest request)
           {
               var headers = request.GetResponseHeaders();
               if (headers != null && headers.ContainsKey("Date"))
               {
                   Date = DateTime.Parse(headers["Date"]);
               }
               else
               {
                   Date = DateTime.Now;
               }
               Debug.Log(
#if UNITY_2017_1_OR_NEWER
                    "isNetworkError:" + request.isNetworkError + "\n" +
                    "isHttpError:" + request.isHttpError + "\n" +
#else
                    "isWebError" + request.isError + "'\n" +
#endif
                    "www.error:'" + request.error + "'\n" +
                   "responseCode:" + request.responseCode
                   );
               if (
#if UNITY_2017_1_OR_NEWER
                    !request.isNetworkError && !request.isHttpError
#else
                    !request.isError
#endif
                     && string.IsNullOrEmpty(request.error) && request.responseCode < 400)
               {
                   // Show results as text
                   string data = request.downloadHandler.text;
                   Response(hash.ToString("X") + data);
               }
               else
               {
                   Debug.Log(request.responseCode + " " + request.error);

                   if (Application.isEditor)
                   {
                       //运行测试方法
                       InvokeTestMethod(RelativeUrl, param, method, getparam, hash);
                   }
                   else
                   {
                       //调用web端方法进行请求
                       try
                       {
                           WebGLInternalCall.Call(requesturl);
                       }
                       catch (Exception e)
                       {
                           Debug.LogException(e);
                       }
                   }

               }
           };
#if false && UNITY_2017_2_OR_NEWER
            asyn.completed += operation => action(operation);
            yield return new WaitForFrame(2);
#else
            while (!asyn.isDone)
            {
                yield return null;
            }
            action(req);
#endif

            #endregion
            /*
            if (Application.isEditor)
            {
                //运行测试方法
                InvokeTestMethod(RelativeUrl, param, method, getparam, hash);
            }
            else
            {
                //调用web端方法进行请求
                try
                {
                    WebGLInternalCall.Call(requesturl);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
            */


            //等待接受命令
            while (String.IsNullOrEmpty(responseDictionary.TryGet(hash)))
            {
                yield return null;
            }
            //完成调用方法
            InvokeResponseByHash(method, hash);
            yield break;
        }



        private void InvokeResponseByHash(CallBack method, int hash)
        {
            string responseText = responseDictionary[hash];
            if (String.IsNullOrEmpty(responseText) || responseText == "[]" || responseText[0].Equals('\n') || responseText[0].Equals('\r') || responseText[0].Equals('\\') || responseText[0].Equals(' ') || responseText[0].Equals('\"'))
            {
                responseText = "error";
            }
            if (method != null)
            {
                string message = "Run " + method.Method.Name;
                if (responseText == "error")
                {
                    message += "," + responseText;
                    Debug.LogError(message);
                }
                else
                {
                    Debug.Log(message);
                }
                method.Invoke(responseText);
            }
        }

        /// <summary>
        /// //组合参数格式
        /// </summary>
        /// <param name="hash"></param>
        /// <param name="RelativeUrl"></param>
        /// <param name="getparam"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        private static string GetRequestUrl(int hash, string RelativeUrl, out string getparam, ref Dictionary<string, string> param)
        {
            //组合参数格式
            getparam = "";
            if (param == null)
            {
                param = new Dictionary<string, string>();
            }
            param.Add("hash", hash.ToString("X"));

            List<string> s = new List<string>();
            foreach (KeyValuePair<string, string> keyValuePair in param)
            {
                s.Add(keyValuePair.Key + "=" + keyValuePair.Value);
            }
            //参数格式
            getparam = String.Join("&", s.ToArray());

            //相对地址的请求格式
            string requesturl = RelativeUrl + "?" + getparam;
            return requesturl;
        }

        private void InvokeTestMethod(string RelativeUrl, Dictionary<string, string> param, CallBack method, string getparam, int hash)
        {
            //本地模拟数据
            var methods = GetType().GetMethods();


            //找到以Test开头的模拟方法
            MethodInfo testMethod = null;
            foreach (MethodInfo info in methods)
            {
                if (info.Name.Contains("Test" + method.Method.Name))
                {
                    testMethod = info;
                }
            }

            if (testMethod != null)
            {
                string response = "error";
                try
                {
                    //调用test方法
                    string invoke = (string)testMethod.Invoke(this, new object[] { param });
                    string fileurl = Regex.Replace(getparam, @"&{0,1}hash=\w+", "", RegexOptions.IgnoreCase);
                    WriteTestFile(RelativeUrl + "-" + fileurl, invoke);
                    response = invoke;
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }

                //发送回调
                string value = hash.ToString("X") + response;
                //StartCoroutine(delayEnumerator(value, () => { SendMessage("Response", value); }));
                Debug.Log("Test测试回调" + value);
                SendMessage("Response", value);

            }
            else
            {
                Debug.Log("没有Test方法" + method.Method.Name);
            }
        }

        IEnumerator delayEnumerator(string value, Action action)
        {
            yield return new WaitForSeconds(Random.Range(0.1f, 1.5f));
            action.Invoke();
        }

        /// <summary>
        /// PC下写后台文件
        /// </summary>
        /// <param name="url"></param>
        /// <param name="data"></param>
        protected virtual void WriteTestFile(string url, string data)
        {

        }

        IEnumerator DownloadData(string url, CallBack method)
        {
            string responseText = "";
            using (WWW www = new WWW(url))
            {
                yield return www;
                if (String.IsNullOrEmpty(www.error))
                {
                    responseText = www.text;
                }
                else
                {
                    Debug.Log(www.error);
                    //TODO txt机柜内容处理 
                    //responseText = "error";
                }
            }
            if (!String.IsNullOrEmpty(responseText) && responseText != "error")
            {
                Debug.Log("TestSceneData\n" + url + "\n" + responseText);
                if (method != null)
                {
                    method.Invoke(responseText);
                }
            }
        }

        #region 虚拟方法

        protected virtual bool isLogin()
        {
            return true;
        }
        public virtual Coroutine Login()
        {
            return null;
        }
        #endregion


        public virtual void HideLoading()
        {

        }
    }
}
