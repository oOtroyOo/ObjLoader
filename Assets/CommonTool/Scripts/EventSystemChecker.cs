using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Linq;
public class EventSystemChecker : MonoBehaviour
{
    //public GameObject eventSystem;

    // Use this for initialization
    void LateUpdate()
    {
        if (EventSystem.current == null)
        {
            //Instantiate(eventSystem);
            GameObject obj = new GameObject("EventSystem");
            obj.AddComponent<EventSystem>();
            obj.AddComponent<StandaloneInputModule>().forceModuleActive = true;
        }
        if (!Array.Exists(Camera.allCameras, c => c.gameObject.GetComponent<PhysicsRaycaster>()))
        {
            if ((Camera.main ?? Camera.current) != null)
            {
                (Camera.main ?? Camera.current).gameObject.AddComponent<PhysicsRaycaster>();
            }
        }
    }
}
