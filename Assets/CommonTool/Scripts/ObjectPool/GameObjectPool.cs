﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CommonTool
{
    public class GameObjectPool : ObjectPool<GameObject>
    {
        public GameObjectPool() : base(null, null, null)
        {
            base.IsEnable = IsGameObjectEnable;
            base.ToEnable = ToEnableGameObject;
            base.CreateNew = CreateNewGameObject;
        }

        private bool IsGameObjectEnable(GameObject gameObject)
        {
            return gameObject.activeSelf;
        }

        private void ToEnableGameObject(GameObject gameObject, bool b)
        {
            gameObject.SetActive(b);
        }

        private GameObject CreateNewGameObject(int count)
        {
            return new GameObject("GameObject" + count);
        }
    }
}
