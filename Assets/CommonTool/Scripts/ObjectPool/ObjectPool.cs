﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

namespace CommonTool
{
    public class ObjectPool<T> where T : UnityEngine.Object
    {
        private Dictionary<object, float> stamp = new Dictionary<object, float>();
        protected List<T> objects = new List<T>();
        protected Action<T, bool> ToEnable;
        protected Func<int, T> CreateNew;
        protected Func<T, bool> IsEnable;
        private ObjectPool()
        {

        }
        public ObjectPool([NotNull]Func<int, T> createNew, Action<T, bool> toEnable = null, Func<T, bool> isEnable = null)
        {
            this.CreateNew = createNew;
            this.ToEnable = toEnable;
            this.IsEnable = isEnable;
        }

        public int Count { get { return objects.Count; } }

        public T Get()
        {
            T obj = objects.FirstOrDefault(o => !IsActive(o)) ?? Create();
            if (ToEnable != null)
            {
                ToEnable(obj, true);
            }

            return obj;
        }

        public void Add(T newObj)
        {
            if (!objects.Contains(newObj))
            {
                objects.Add(newObj);
                stamp.Add(newObj, Time.time);
            }
        }
        public T[] Gets(int count)
        {
            T[] objs = new T[count];
            for (int i = 0; i < count; i++)
            {
                objs[i] = Get();
            }
            return objs;
        }

        public void Disable(T obj)
        {
            if (ToEnable != null)
            {
                ToEnable(obj, false);
            }
            objects.Sort(Comparison);
        }

        public void DisableAll()
        {
            foreach (T o in objects)
            {
                Disable(o);
            }
        }

        private T Create()
        {

            T obj = CreateNew(objects.Count);
            stamp.Add(obj, Time.time);
            objects.Add(obj);
            return obj;
        }

        private int Comparison(T x, T y)
        {
            int a = IsActive(x).CompareTo(IsActive(y));
            if (a == 0)
            {
                return (stamp[x].CompareTo(stamp[y]));
            }
            return a;
        }

        private bool IsActive(T obj)
        {
            if (IsEnable != null)
            {
                return IsEnable(obj);
            }
            if (obj is GameObject)
            {
                return (obj as GameObject).activeSelf;
            }
            if (obj is Behaviour)
            {
                return (obj as Behaviour).isActiveAndEnabled;
            }
            if (obj is Component)
            {
                return (obj as Component).gameObject.activeSelf;
            }
            return false;
        }

    }
}
