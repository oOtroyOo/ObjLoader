﻿
using UnityEngine;
public class WaitForFrame : CustomYieldInstruction
{
    private readonly int _count;
    private readonly int frame;

    public WaitForFrame() : this(2)
    {

    }

    public WaitForFrame(int count)
    {
        frame = Time.frameCount;
        _count = Mathf.Clamp(count, 2, int.MaxValue);
    }
    public override bool keepWaiting
    {
        get { return Time.frameCount >= frame + _count; }
    }
}
