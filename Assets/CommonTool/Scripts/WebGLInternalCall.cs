﻿
#if UNITY_WEBGL && !UNITY_EDITOR
//#define WEBGL
#endif

using UnityEngine;

namespace CommonTool
{
    public class WebGLInternalCall
    {
#if WEBGL
    [DllImport("__Internal")]
    extern
#endif
        public static void UnityLog(string text)
#if WEBGL
        ;
#else
        {
            ExternalCall("UnityLog", text);
        }
#endif
        //=====================================
#if WEBGL
    [DllImport("__Internal")]
    extern
#endif
        public static void WebCallTest(string text)
#if WEBGL
        ;
#else
        {
            ExternalCall("WebCallTest", text);
        }
#endif
        //=====================================
#if WEBGL
    [DllImport("__Internal")]
    extern
#endif


        //=====================================
#if WEBGL
    [DllImport("__Internal")]
    extern
#endif
        public static void HideLoading()
#if WEBGL
        ;
#else
        {
            ExternalCall("HideLoading");
        }
#endif

        //=====================================
#if WEBGL
    [DllImport("__Internal")]
    extern
#endif
        public static void Call(string text)
#if WEBGL
        ;
#else
        {
            ExternalCall("Call", text);
        }
#endif

        //=====================================
#if WEBGL
    [DllImport("__Internal")]
    extern
#endif
        public static void toLinkChart(string text)
#if WEBGL
        ;
#else
        {
            ExternalCall("toLinkChart", text);
        }
#endif


        //=====================================
#if WEBGL
    [DllImport("__Internal")]
    extern
#endif
        public static void toLinkChart()
#if WEBGL
        ;
#else
        {
            ExternalCall("toLinkChart");
        }
#endif

        //=====================================
#if WEBGL
    [DllImport("__Internal")]
    extern
#endif
        public static void toLinkWarning(string text)
#if WEBGL
        ;
#else
        {
            ExternalCall("toLinkWarning", text);
        }
#endif

        //=====================================
#if WEBGL
    [DllImport("__Internal")]
    extern
#endif
        public static void toLinkWarning()
#if WEBGL
        ;
#else
        {
            ExternalCall("toLinkWarning");
        }
#endif


        //=====================================
#if WEBGL
    [DllImport("__Internal")]
    extern
#endif
        public static void toLinkBreak(string text)
#if WEBGL
        ;
#else
        {
            ExternalCall("toLinkBreak", text);
        }
#endif
        //=====================================

#if WEBGL
    [DllImport("__Internal")]
    extern
#endif
        public static void toLinkBreak()
#if WEBGL
        ;
#else
        {
            ExternalCall("toLinkBreak");
        }
#endif

        public static void ExternalCall(string method, string data = null)
        {
            Debug.Log("Unity ExternalCall,method:" + method + ",data:" + (data == null ? "" : data));
            if (data == null)
            {
                Application.ExternalCall(method);
            }
            else
            {
                Application.ExternalCall(method, data);
            }
        }
    }
}
