﻿using LitJson;
using System;
using UnityEngine;

#if UNITY_EDITOR

#endif

namespace CommonTool
{


    internal class SerializerXML : ISerializerInterface
    {
        public T FromData<T>(string value)
        {
            return XMLUtils.DeserializeObject<T>(value);

            return (T)FromData(value, typeof(T));
        }

        public string ToDataString<T>(T obj)
        {
            return XMLUtils.SerializeObject(obj);
            return ToDataString(obj, typeof(T));
        }

        public object FromData(string value, Type type)
        {
            return XMLUtils.DeserializeObject(value, type);
        }

        public string ToDataString(object obj, Type type)
        {
            return XMLUtils.SerializeObject(obj, type);
        }
    }
    internal class SerializerJson : ISerializerInterface
    {
        public T FromData<T>(string value)
        {
            try
            {
                return JsonUtility.FromJson<T>(value);
            }
            catch (Exception)
            {


            }
            try
            {
                JsonMapper.ToObject<T>(value);
            }
            catch (Exception e)
            {

            }
            try
            {
                return (T)FromData(value, typeof(T));
            }
            catch (Exception e)
            {
                Debug.LogException(e);

            }
            return default(T);
        }

        public string ToDataString<T>(T obj)
        {
            JsonMapper.ToJson(obj);
            return JsonUtility.ToJson(obj);
        }

        public object FromData(string value, Type type)
        {
            try
            {
                return JsonUtility.FromJson(value, type);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string ToDataString(object obj, Type type)
        {
           return LitJson.JsonMapper.ToJson(obj);
            //throw new NotImplementedException();
        }
    }
    public class Serializer
    {
        public static ISerializerInterface Xml = new SerializerXML();

        public static ISerializerInterface Json = new SerializerJson();
        private static ISerializerInterface defaultSerializer = Xml;
        //{
        //    get
        //    {
        //        switch (DefautType)
        //        {
        //            case SerializerType.xml:
        //                return Xml;
        //                break;
        //            case SerializerType.json:
        //                return Json;
        //                break;

        //            default:
        //                throw new ArgumentOutOfRangeException();
        //        }
        //    }
        //}

        //public static SerializerType DefautType = SerializerType.xml;
        //public enum SerializerType
        //{
        //    xml, json
        //}


        public static string ToDataString<T>(T obj)
        {
            return defaultSerializer.ToDataString(obj);
        }


        public static string ToDataString(object obj, Type type)
        {
            return defaultSerializer.ToDataString(obj, type);

        }

        public static T FromData<T>(string value)
        {
            try
            {
                if (value.StartsWith("{"))
                {
                    return Json.FromData<T>(value);
                }
                else if (value.StartsWith("<"))
                {
                    Xml.FromData<T>(value);
                }
                throw new TypeLoadException("数据串未包含{ 或< 开头");
            }
            catch (Exception e)
            {
                Debug.LogException(e/*,"数据反序列化失败"*/);
            }
            return default(T);
        }
        public static object FromData(string value, Type type)
        {
            if (value.StartsWith("{"))
            {
                try
                {
                    return Json.FromData(value, type);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
            else if (value.StartsWith("<"))
            {
                try
                {
                    return Xml.FromData(value, type);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
            Debug.LogError("数据反序列化失败" + type.Name);
            return null;
        }

    }
}

