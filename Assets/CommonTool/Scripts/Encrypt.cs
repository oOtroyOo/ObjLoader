﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class Encrypt : MonoBehaviour
{
   

    /// <summary>
    /// 将文本数据加密并写入文件，
    /// 将value通过UTF-8编码转换为byte[]再进行加密，并输出fileBytes保存
    /// </summary>
    /// <param name="path">保存路径</param>
    /// <param name="value">文本数据</param>
    public static void EncryptFile(string PASSWORD_CRYPT_KEY,string path, string value)
    {

        byte[] fileBytes = Encode(PASSWORD_CRYPT_KEY,Encoding.UTF8.GetBytes(value));
        FileStream cfs = new FileStream(path, FileMode.Create);
        cfs.Write(fileBytes, 0, fileBytes.Length);
        cfs.Close();
    }

    /// <summary>
    /// 读取指定文档，进行解密，并返回文本string。
    /// 采用byte[]读取文件，进行解密，将数据编写为UTF-8格式字符串
    /// </summary>
    /// <param name="path">文档路径</param>
    /// <returns></returns>
    public static string DecryptFile(string PASSWORD_CRYPT_KEY,string path)
    {
        if (!File.Exists(path))
        {
            return "";
        }
        FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
        byte[] fileBytes = new byte[fs.Length];
        fs.Read(fileBytes, 0, (int)fs.Length);
        fileBytes = Decode(PASSWORD_CRYPT_KEY,fileBytes);
        fs.Close();
        string outstr = Encoding.UTF8.GetString(fileBytes);
        return outstr;

    }

    /// <summary>
    /// 将String value通过UTF-8编码转换为byte[]再进行加密
    /// </summary>
    /// <param name="value">文本数据</param>
    public static byte[] EncryptString(string PASSWORD_CRYPT_KEY, string value)
    {

        byte[] Bytes = Encode(PASSWORD_CRYPT_KEY, Encoding.UTF8.GetBytes(value));
        return Bytes;
    }

    /// <summary>
    /// 进行解密byte[]，并返回文本UTF-8格式String
    /// <returns></returns>
    public static string DecryptString(string PASSWORD_CRYPT_KEY, byte[] bytes)
    {
        bytes = Decode(PASSWORD_CRYPT_KEY, bytes);
        string outstr = Encoding.UTF8.GetString(bytes);
        return outstr;
    }


    ///<summary>字节DES加密函数 , 可将各种类型的对象转换为byte[]进行加密</summary>
    ///<param name="bytes">需加密字节</param>    
    ///<returns>加密完成后的字节</returns> 
    public static byte[] Encode(string PASSWORD_CRYPT_KEY,byte[] bytes)
    {

        //   if (bytes.Length < 1048576)
        {
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            provider.Key = Encoding.ASCII.GetBytes(PASSWORD_CRYPT_KEY.Substring(0, 8));
            provider.IV = Encoding.ASCII.GetBytes(PASSWORD_CRYPT_KEY.Substring(0, 8));
            MemoryStream stream = new MemoryStream();
            CryptoStream stream2 = new CryptoStream(stream, provider.CreateEncryptor(), CryptoStreamMode.Write);
            stream2.Write(bytes, 0, bytes.Length);
            stream2.FlushFinalBlock();
            stream.Close();
            return stream.ToArray();
        }
        //else
        //{
        //    int max = 1024;
        //    for (int i = 0; i < max; i++)
        //    {
        //        bytes[i] = (byte)~bytes[i];
        //    }
        //    return bytes;
        //}
    }


    ///<summary>字节DES解密函数 , 可适用于各种来源的byte[]进行解密</summary>
    ///<param name="bytes">被解密字节</param>    
    ///<returns>解密完成后的字节</returns> 
    public static byte[] Decode(string PASSWORD_CRYPT_KEY,byte[] bytes)
    {

        //if (bytes.Length < 1048576)
        {
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            provider.Key = Encoding.ASCII.GetBytes(PASSWORD_CRYPT_KEY.Substring(0, 8));
            provider.IV = Encoding.ASCII.GetBytes(PASSWORD_CRYPT_KEY.Substring(0, 8));
            MemoryStream stream = new MemoryStream();
            CryptoStream stream2 = new CryptoStream(stream, provider.CreateDecryptor(), CryptoStreamMode.Write);
            stream2.Write(bytes, 0, bytes.Length);
            bool noError = true;
            try
            {
                stream2.FlushFinalBlock();
                noError = true;
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
                noError = false;
            }

            stream.Close();
            if (noError == false)
            {
                return new byte[] { 0 };
            }
            return stream.ToArray();
        }
        //else
        //{
        //    int max = 1024;
        //    for (int i = 0; i < max; i++)
        //    {
        //        bytes[i] = (byte)~bytes[i];
        //    }
        //    return bytes;
        //}
    }
}
