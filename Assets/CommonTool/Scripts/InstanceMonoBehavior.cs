using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CommonTool
{
    /// <summary>
    /// T 写自身类名
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class InstanceMonoBehavior<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        public static T GetInstance()
        {
            if (_instance == null)
            {

                _instance = GameObject.FindObjectsOfType<T>().FirstOrDefault();
                if (_instance != null)
                {
                    return _instance;
                }
                GameObject go = new GameObject(typeof(T).Name);
                _instance = go.AddComponent<T>();
            }

            return _instance;
        }

        /// <summary>
        /// 子类需要使用Awake时，写：
        /// 
        ///<para>protected override void Awake()</para>
        ///<para>{                              </para>
        ///<para>    base.Awake();              </para>
        ///<para>    ...                        </para>
        ///<para>}                              </para>
        ///
        /// </summary>
        protected virtual void Awake()
        {

            if (typeof(T) != GetType())
            {
                throw new InvalidCastException(typeof(T) + "=/=" + _instance.GetType());
            }
            if (_instance != null && _instance != this)
            {
                Destroy(this);
                return;
            }
            _instance = this as T;
        }
    }
}