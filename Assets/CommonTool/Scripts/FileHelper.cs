﻿using System.IO;
using System.Text;
using UnityEngine;

namespace CommonTool
{
    public class FileHelper
    {
        public static StreamWriter CreatWriter(FileInfo fi, out FileStream fs)
        {
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
            }
            FileInfo[] oldFiles = fi.Directory.GetFiles();
            if (oldFiles.Length > 9)
            {
                for (int i = 0; i < oldFiles.Length - 9; i++)
                {
                    File.Delete(oldFiles[i].FullName);
                }
            }
            fs = new FileStream(fi.FullName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);

            Debug.Log("创建文件" + fi.Name);



            var streamWriter = new StreamWriter(fs, Encoding.UTF8);

            streamWriter.Flush();
            return streamWriter;

        }
        public static StreamWriter CreatWriter(string filename, out FileStream fs)
        {
            return CreatWriter(new FileInfo(filename), out fs);
        }
    }
}
