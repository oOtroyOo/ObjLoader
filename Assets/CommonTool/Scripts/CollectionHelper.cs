﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CommonTool
{



    /// <summary>
    /// 集合或数组助手类
    /// </summary>
    public static class CollectionHelper
    {
        public delegate TKey SelectHandler<TSource, TKey>(TSource source);

        public delegate bool FindHandler<T>(T item);

        /// <summary>
        /// 升序排序(选择排序)
        /// </summary>
        /// <param name="array">需要排序的数组</param>
        /// <param name="handler">排序的根据</param>
        public static void OrderBy<T, TKey>(T[] array, SelectHandler<T, TKey> handler)
            where TKey : IComparable
        {
            int min = -1;
            for (int i = 0; i < array.Length - 1; i++)
            {
                min = i;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (handler(array[min]).CompareTo(handler(array[j])) > 0)
                        min = j;
                }
                if (min > i)
                {
                    var temp = array[i];
                    array[i] = array[min];
                    array[min] = temp;
                }
            }
        }

        /// <summary>
        /// 降序排序(选择排序)
        /// </summary>
        /// <param name="array">需要排序的数组</param>
        /// <param name="handler">排序的根据</param>
        public static void OrderByDescending<T, TKey>(T[] array, SelectHandler<T, TKey> handler)
            where TKey : IComparable
        {
            int min = -1;
            for (int i = 0; i < array.Length - 1; i++)
            {
                min = i;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (handler(array[min]).CompareTo(handler(array[j])) < 0)
                        min = j;
                }
                if (min > i)
                {
                    var temp = array[i];
                    array[i] = array[min];
                    array[min] = temp;
                }
            }
        }

        /// <summary>
        /// 查找单个对象
        /// </summary>
        /// <param name="handler">比较条件</param>
        public static T Find<T>(T[] array, FindHandler<T> handler)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (handler(array[i]))
                    return array[i];
            }
            return default(T);
        }

        /// <summary>
        /// 查找多个对象
        /// </summary>
        /// <param name="handler">比较条件</param>
        public static T[] FindAll<T>(T[] array, FindHandler<T> handler)
        {
            //准备一个集合，用于存放找到的对象
            List<T> tempList = new List<T>();
            for (int i = 0; i < array.Length; i++)
            {
                if (handler(array[i])) //判断查找条件
                    tempList.Add(array[i]);
            }
            //如果找到，返回找到的元素组成的数组，没找到，返回空
            return tempList.Count > 0 ? tempList.ToArray() : null;
        }

        /// <summary>
        /// 选择
        /// </summary>
        /// <param name="array">源数组</param>
        /// <param name="handler">选择算法</param>
        /// <returns></returns>
        public static TKey[] Select<T, TKey>(T[] array, SelectHandler<T, TKey> handler)
        {
            TKey[] tempArr = new TKey[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                tempArr[i] = handler(array[i]);
            }
            return tempArr;
        }

        /// <summary> 取一组不重复随机数 </summary>
        public static int[] GetRandomArray(int minNum, int maxNum, int many)
        {
            if (many > (maxNum - minNum) + 1)
            {
                throw new Exception("要取的个数:many 大于可取个数");
            }
            int j;
            int[] b = new int[many];
            System.Random r = new System.Random();
            for (j = 0; j < many; j++)
            {
                int i = r.Next(minNum, maxNum + 1);
                int num = 0;
                for (int k = 0; k < j; k++)
                {
                    if (b[k] == i)
                    {
                        num = num + 1;
                    }
                }
                if (num == 0)
                {
                    b[j] = i;
                }
                else
                {
                    j = j - 1;
                }
            }
            return b;
        }
    }
    public class ExtentionMath
    {
        public static float DeltaAngle(float newAngle, float oldAngle)
        {
            //current = (current % 360 + 180) % 360 - 180;
            //last = (last % 360 + 180) % 360 - 180;
            //if (Mathf.Abs(current - last) > 180)
            //{
            //    return -360 - (current - last);
            //}
            float delta = ((newAngle - oldAngle) % 360 + 180 + 360) % 360 - 180;
            if (Mathf.Abs(delta) > 180)
            {
                Debug.LogError(delta);
            }
            return delta;

        }
    }
    public static class DictionaryExtension
    {

        /// <summary>
        /// 尝试根据key得到value，得到了的话直接返回value，没有得到直接返回null
        /// this Dictionary<Tkey,Tvalue> dict 这个字典表示我们要获取值的字典
        /// </summary>
        public static Tvalue TryGet<Tkey, Tvalue>(this Dictionary<Tkey, Tvalue> dict, Tkey key)
        {
            Tvalue value;
            dict.TryGetValue(key, out value);
            return value;
        }

        public static void AddValue<Tkey, Tvalue>(this Dictionary<Tkey, Tvalue> dict, Tkey key, Tvalue value)
        {

            if (dict.ContainsKey(key))
            {
                dict[key] = value;
            }
            else
            {
                dict.Add(key, value);
            }
        }
    public static Tvalue GetObject<Tkey, Tvalue>(this Dictionary<Tkey, Tvalue> dic, Tkey key) where Tvalue : class, new()
    {
        if (!dic.ContainsKey(key))
        {
            dic.Add(key, new Tvalue());
        }
        return dic[key];
    }


    }
}