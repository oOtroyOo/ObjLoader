﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
#endif

namespace CommonTool
{



    public class LoadingUI : MonoBehaviour
    {
        public Scrollbar[] Scrollbars;
        public Slider[] Sliders;
        public Image[] ImageFills;
        public Animator[] Animators;
        public Text[] Texts;
        public Text Title;

        public MonoBehaviour[] OtherBehaviours;
        public string[] OtherParamName;


        private Dictionary<Text, string> orgText = new Dictionary<Text, string>();
        private bool isInit = false;


        // Use this for initialization
        void Awake()
        {
            DontDestroyOnLoad(gameObject);
            foreach (Text text in Texts)
            {
                orgText.Add(text, text.text);
            }
            if (Title != null)
            {
                orgText.Add(Title, Title.text);
            }
            Init();
        }

        // Update is called once per frame
        void Update()
        {
            if (isInit)
            {
                Init();
            }
        }

        void Init()
        {
            if (Loading.Instance != null)
            {
                Loading.Instance.onLoadingEvent += (OnLoading);
                isInit = true;

            }
        }

        void OnLoading(string sceneName, float progress)
        {
            if (progress < 1)
            {
                gameObject.SetActive(true);

                if (Title != null)
                {
                    Title.text = !string.IsNullOrEmpty(orgText[Title]) ? string.Format(orgText[Title], sceneName) : sceneName;
                }
                foreach (Text text in Texts)
                {
                    text.text = !string.IsNullOrEmpty(orgText[text]) ? string.Format(orgText[text], progress) : progress.ToString("f2");
                }

                foreach (Image image in ImageFills)
                {
                    if (image.type == Image.Type.Filled)
                    {
                        image.fillAmount = progress;
                    }
                }

                foreach (Scrollbar scrollbar in Scrollbars)
                {
                    scrollbar.value = progress;
                }
                foreach (Slider slider in Sliders)
                {
                    slider.value = progress;
                }
                foreach (Animator animator in Animators)
                {
                    animator.speed = 1;
                }

                for (int i = 0; i < OtherBehaviours.Length; i++)
                {
                    FieldInfo field = OtherBehaviours[i].GetType().GetField(OtherParamName[i]);
                    PropertyInfo prop = OtherBehaviours[i].GetType().GetProperty(OtherParamName[i]);
                    if (field != null)
                    {
                        field.SetValue(OtherBehaviours[i], progress);
                    }
                    if (prop != null)
                    {
                        prop.SetValue(OtherBehaviours[i], progress, null);
                    }
                }
            }
            else
            {
                foreach (Animator animator in Animators)
                {
                    animator.speed = 0;
                }
                gameObject.SetActive(false);
            }
        }

    }
}
