﻿
#if (UNITY_WEBGL||UNITY_WEBPLAYER)&&!UNITY_EDITOR
#define WEB
#endif

#if !WEB
using System.IO;
#endif
using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text;
using UnityEngine;
using UnityEngine.UI;


namespace CommonTool
{
    public class LogWritter
    {
#if !WEB
        private static FileStream logfs;
        private static FileStream logErfs;
        private static StreamWriter logStream;
        private static StreamWriter logErStream;
        private static FileInfo logfi;
        private static FileInfo logErfi;
#endif
        public static string filePath = Application.persistentDataPath + "/log/log.txt";
        public static string fileErrPath = Application.persistentDataPath + "/log/logError.txt";
        public static bool useColor = true;
        private static Text UItext;


        [RuntimeInitializeOnLoadMethod]
        public static void Load()
        {

            if (Debug.isDebugBuild && !Application.isEditor)
            {
                Application.logMessageReceived += WriteUI;
            }
#if !WEB
            WriteLogCreate();
            Application.logMessageReceived += WriteLog;
#endif

        }

        private static void WriteUI(string condition, string stacktrace, LogType type)
        {
            if (UItext == null)
            {
                GameObject debugcanvas = Resources.Load<GameObject>("DebugCanvas");
                if (debugcanvas != null)
                {
                    debugcanvas = GameObject.Instantiate(debugcanvas);
                    UnityEngine.Object.DontDestroyOnLoad(debugcanvas);
                    UItext = debugcanvas.GetComponentInChildren<Text>();
                    debugcanvas.AddComponent<EventSystemChecker>();
                    Resources.UnloadUnusedAssets();
                }
                else
                {
                    UItext = CreateUI();
                }
                Shadow s = UItext.gameObject.GetComponent<Shadow>() ?? UItext.gameObject.AddComponent<Shadow>();
            }

            if (Debug.isDebugBuild || type == LogType.Exception)
            {
                UItext.text = MakeLogConsoleString(condition, stacktrace, type);
            }
        }

        public static string MakeLogConsoleString(string condition, string stacktrace, LogType type)
        {
            StringBuilder uiTextStringBuilder = new StringBuilder();

            string newline = @condition + "\n" + (useColor ? "<color=cyan>" : "") + stacktrace + (useColor ? "</color>" : "");
            if (type == LogType.Warning)
            {
                newline = (useColor ? "<color=yellow>" : "") + newline + (useColor ? "</color>" : "");
            }
            else if (type == LogType.Error || type == LogType.Exception)
            {
                newline = (useColor ? "<color=red>" : "") + newline + (useColor ? "</color>" : "");
            }

            uiTextStringBuilder.AppendLine(newline);
            uiTextStringBuilder.AppendLine();
            int lenth = 4000;
            if (uiTextStringBuilder.Length > lenth)
            {
                uiTextStringBuilder.Remove(0, uiTextStringBuilder.Length - lenth);
            }
            return uiTextStringBuilder.ToString();
        }

        public static void Quit()
        {

        }

#if !WEB
        public static void WriteLogCreate()
        {
            try
            {
                if (logfs == null)
                {
                    if (logfi == null)
                    {
                        //string filePath = AppData.resPath + "log/" + (string.IsNullOrEmpty(CourseId) ? "Student" : CourseId) + "_" + DateTime.Now.ToString("yyyy-MM-dd_HHmmss") + ".log";
                        logfi = new FileInfo(filePath);
                    }

                    if (!logfi.Directory.Exists)
                    {
                        logfi.Directory.Create();
                    }
                    FileInfo[] oldFiles = logfi.Directory.GetFiles();
                    if (oldFiles.Length > 9)
                    {
                        for (int i = 0; i < oldFiles.Length - 9; i++)
                        {
                            File.Delete(oldFiles[i].FullName);
                        }
                    }
                    logfs = new FileStream(logfi.FullName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);

                    Debug.Log("创建log文件" + logfi.FullName);


                }
                if (logStream == null)
                {
                    logStream = new StreamWriter(logfs, Encoding.UTF8);
                    logStream.WriteLine("开始记录");
                    logStream.Flush();
                }
                if (logErfs == null)
                {
                    if (logErfs == null)
                    {
                        //filePath = AppData.resPath + "log/Error/" + (string.IsNullOrEmpty(CourseId) ? "Student" : CourseId) + "_" + DateTime.Now.ToString("yyyy-MM-dd_HHmmss") + "_Error.log";
                        logErfi = new FileInfo(fileErrPath);
                    }

                    if (!logErfi.Directory.Exists)
                    {
                        logErfi.Directory.Create();
                    }
                    FileInfo[] oldFiles = logErfi.Directory.GetFiles();
                    if (oldFiles.Length > 9)
                    {
                        for (int i = 0; i < oldFiles.Length - 9; i++)
                        {
                            File.Delete(oldFiles[i].FullName);
                        }
                    }
                    logErfs = new FileStream(logErfi.FullName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);

                    Debug.Log("创建logError文件" + logErfi.FullName);

                }
                if (logErStream == null)
                {
                    logErStream = new StreamWriter(logErfs, Encoding.UTF8);
                    logErStream.WriteLine("开始记录");
                    logErStream.Flush();
                }
                ApplicationHandler.GetInstance().onApplcationQuit.AddListener(WriteLogClose);
            }
            catch (Exception e)
            {
                throw e;

            }
        }
        public static void WriteLogClose()
        {
            if (logStream != null)
            {
                logStream.Close();
                logStream = null;
            }
            if (logfs != null)
            {
                logfs.Close();
                logfs = null;
            }

            if (logErStream != null)
            {
                logErStream.Close();
                logErStream = null;
            }
            if (logErfs != null)
            {
                logErfs.Close();
                logErfs = null;
            }
        }
        private static void WriteLog(string condition, string stackTrace, LogType type)
        {

            List<StreamWriter> sw = new List<StreamWriter>();
            if (logStream != null)
            {
                sw.Add(logStream);
            }
            if (logErStream != null && type != LogType.Log)
            {
                sw.Add(logErStream);
            }
            foreach (StreamWriter streamWriter in sw)
            {
                try
                {
                    streamWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss.ff"));
                    streamWriter.WriteLine(type);
                    streamWriter.WriteLine(condition);
                    streamWriter.WriteLine(stackTrace);
                    streamWriter.WriteLine();
                    streamWriter.Flush();
                }
                catch (Exception e)
                {

                }
            }
        }
#endif
        private static Text CreateUI()
        {
            GameObject canvasGo = new GameObject("DebugCanvas", typeof(RectTransform), typeof(Canvas), typeof(CanvasScaler), typeof(GraphicRaycaster));
            Canvas canvas = canvasGo.GetComponent<Canvas>();
            canvas.sortingOrder = 1000;
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            CanvasScaler scaler = canvasGo.GetComponent<CanvasScaler>();
            scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            scaler.referenceResolution = new Vector2(1920, 1080);

            GameObject TextGo = new GameObject("Text", typeof(RectTransform));
            Text text = TextGo.AddComponent<Text>();
            text.raycastTarget = false;
            ContentSizeFitter filter = TextGo.AddComponent<ContentSizeFitter>();
            filter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
            RectTransform textTransform = text.transform as RectTransform;
            textTransform.SetParent(canvasGo.transform, false);
            textTransform.anchorMin = Vector2.zero;
            textTransform.anchorMax = new Vector2(1, 0);
            textTransform.pivot = new Vector2(0.5f, 0);
            textTransform.offsetMax = Vector2.zero;
            textTransform.offsetMin = Vector2.zero;
            text.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            text.fontSize = 25;
            text.SetAllDirty();

            canvasGo.AddComponent<EventSystemChecker>();
            return text;

        }
    }
}
