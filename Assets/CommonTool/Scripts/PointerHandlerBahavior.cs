﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Events;
using UnityEngine.UI;


public class PointerHandlerBahavior : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerDownHandler, IPointerExitHandler, IPointerUpHandler, IDragHandler, IBeginDragHandler, IEndDragHandler, ISubmitHandler, IDropHandler, IScrollHandler
{
    public event Action<PointerEventData> OnPointerEnter_Event;
    public event Action<PointerEventData> OnPointerClick_Event;
    public event Action<PointerEventData> OnPointerDown_Event;
    public event Action<PointerEventData> OnPointerExit_Event;
    public event Action<PointerEventData> OnPointerUp_Event;
    public event Action<PointerEventData> OnDrag_Event;
    public event Action<PointerEventData> OnBeginDrag_Event;
    public event Action<PointerEventData> OnEndDrag_Event;
    public event Action<PointerEventData> OnDrop_Event;
    public event Action<PointerEventData> OnScroll_Event;
    public event Action<BaseEventData> OnSubmit_Event;
    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        if (OnPointerEnter_Event != null)
        {
            Debug.Log(gameObject.name + ",OnPointerEnter:");
            OnPointerEnter_Event.Invoke(eventData);
        }
    }

    public virtual void OnPointerClick(PointerEventData eventData)
    {
        if (OnPointerClick_Event != null)
        {
            Debug.Log(gameObject.name + ",OnPointerClick:");
            OnPointerClick_Event.Invoke(eventData);
        }
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        if (OnPointerDown_Event != null)
        {
            Debug.Log(gameObject.name + ",OnPointerDown:");//  + eventData);
            OnPointerDown_Event.Invoke(eventData);
        }
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        if (OnPointerExit_Event != null)
        {
            Debug.Log(gameObject.name + ",OnPointeExit:");//  + eventData);
            OnPointerExit_Event.Invoke(eventData);
        }
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
        if (OnPointerUp_Event != null)
        {
            Debug.Log(gameObject.name + ",OnPointerUp:");//  + eventData);
            OnPointerUp_Event.Invoke(eventData);
        }
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        if (OnDrag_Event != null)
        {
            Debug.Log(gameObject.name + ",OnDragUp:");//  + eventData);
            OnDrag_Event.Invoke(eventData);
        }
    }

    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        if (OnBeginDrag_Event != null)
        {
            Debug.Log(gameObject.name + ",OnBeginDrag:");//  + eventData);
            OnBeginDrag_Event.Invoke(eventData);
        }
    }

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        if (OnEndDrag_Event != null)
        {
            Debug.Log(gameObject.name + ",OnEndDrag:");//  + eventData);
            OnEndDrag_Event.Invoke(eventData);
        }
    }

    public virtual void OnSubmit(BaseEventData eventData)
    {
        if (OnSubmit_Event != null)
        {
            Debug.Log(gameObject.name + ",OnSubmit:");//  + eventData);
            OnSubmit_Event.Invoke(eventData);
        }
    }

    public virtual void OnDrop(PointerEventData eventData)
    {
        if (OnDrop_Event != null)
        {
            Debug.Log(gameObject.name + ",OnDrop:");// + eventData);
            OnDrop_Event.Invoke(eventData);
        }
    }

    public virtual void OnScroll(PointerEventData eventData)
    {
        if (OnScroll_Event != null)
        {
            Debug.Log(gameObject.name + ",OnScroll:");
            OnScroll_Event.Invoke(eventData);
        }
    }
}
