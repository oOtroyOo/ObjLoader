﻿
using System.IO;
using UnityEditor;
using UnityEngine;


public class CreatBigAsset : Editor
{
    [MenuItem("Assets/Example/Creat Big Asset")]
    static void TestCreateBigAsset()
    {
        string directory = "Assets/CommonTool/Example/BigAsset";
        if (!Directory.Exists(directory))
        {
            Directory.CreateDirectory(directory);
        }
        int fileCount = Directory.GetFiles(directory).Length / 2;
        Debug.Log("等待创建随机文件");

        for (int i = 0; i < fileCount + 1; i++)
        {
            string filename = "asset" + i + ".bytes";
            using (FileStream fs = File.Exists(directory + "/" + filename) ? new FileStream(directory + "/" + filename, FileMode.Truncate) : File.Create(directory + "/" + filename))
            {
                byte[] buffer = new byte[1024 * 1024];
                int max = UnityEngine.Random.Range(20, 100);
                for (int m = 0; m < max; m++)
                {
                    for (int j = 0; j < 1024 * 1024; j++)
                    {
                        buffer[j] = (byte)(UnityEngine.Random.Range(0, 1f) * 255f);
                    }
                    fs.Write(buffer, 0, buffer.Length);

                }
                Debug.Log("创建" + directory + "/" + filename + "文件大小" + max + "MB");
            }
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
            AssetImporter imp = AssetImporter.GetAtPath(directory + "/" + filename);
            imp.assetBundleName = "bigasset" + i + ".unity3d";
        }

    }

    [MenuItem("Assets/Example/Creat Big Image")]
    static void TestCreateBigImage()
    {
        string directory = "Assets/CommonTool/Example/BigImage";
        if (!Directory.Exists(directory))
        {
            Directory.CreateDirectory(directory);
        }
        int fileCount = Directory.GetFiles(directory).Length / 2;
        Debug.Log("等待创建随机图片");

        for (int i = 0; i < fileCount + 1; i++)
        {
            string filename = "image" + i + ".png";
            Texture2D tex = new Texture2D(2048, 2048);
            Color[] colors = new Color[2048 * 2048];
            using (FileStream fs = File.Exists(directory + "/" + filename) ? new FileStream(directory + "/" + filename, FileMode.Truncate) : File.Create(directory + "/" + filename))
            {

                for (int j = 0; j < 2048 * 2048; j++)
                {
                    colors[j].a = 1;
                    colors[j].r = Random.Range(0, 1f);
                    colors[j].g = Random.Range(0, 1f);
                    colors[j].b = Random.Range(0, 1f);
                }
                tex.SetPixels(colors);
                tex.Apply();
                byte[] bytes = tex.EncodeToPNG();
                fs.Write(bytes, 0, bytes.Length);
                Debug.Log("创建" + directory + "/" + filename);
            }
            DestroyImmediate(tex);
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
            AssetImporter imp = AssetImporter.GetAtPath(directory + "/" + filename);
            imp.assetBundleName = "bigimage" + i + ".unity3d";
        }

    }

}

