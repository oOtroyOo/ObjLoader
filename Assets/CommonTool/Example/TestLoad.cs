﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CommonTool;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TestLoad : MonoBehaviour
{
    public string sceneName;

    void Start()
    {
#if UNITY_2017_OR_NEWER || UNITY_2017
        Caching.ClearCache();
#else
        Caching.CleanCache();
#endif
        if (!string.IsNullOrEmpty(sceneName))
        {
            Loading.Instance.LoadLevel(sceneName);
        }
    }

}

