﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CommonTool;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class ObjMeshData
{
    public string mtllib;
    public string meshname;
    public List<string> usemtl = new List<string>();
    public List<Vector3> vertices = new List<Vector3>();
    public List<Vector3> normals = new List<Vector3>();
    public List<Vector3> uvs = new List<Vector3>();
    public List<Face> faces = new List<Face>();



    private Mesh mesh;
    private bool isMeshDone;
    public bool isDone;
    public class Face
    {
        public int usemtl = -1;
        public int[] vertices = new int[3];
        public int[] normals = new int[3];
        public int[] uvs = new int[3];
    }


    public void AddFace(List<int[]> facedata)
    {
        Face face = new Face();
        for (int i = 0; i < facedata.Count; i++)
        {
            face.usemtl = usemtl.Count - 1;
            int[] list = facedata[i];

            face.vertices[i] = list[0];
            face.normals[i] = list[1];
            face.uvs[i] = list[2];

        }
        faces.Add(face);
    }

    public Mesh GetMesh()
    {
        if (mesh == null)
        {
            mesh = new Mesh();
            Application.logMessageReceivedThreaded += ApplicationOnLogMessageReceivedThreaded;
            Thread thred = new Thread(GetMeshThread);
            thred.Start();
        }
        if (isMeshDone)
        {
            return mesh;
        }
        return null;
    }

    private void ApplicationOnLogMessageReceivedThreaded(string condition, string stackTrace, LogType type)
    {
        if (type == LogType.Error || type == LogType.Exception)
        {
            string str = LogWritter.MakeLogConsoleString(condition, stackTrace, type);
            Debug.Log(str);
        }
    }

    void GetMeshThread()
    {
        List<int> meshVertics = new List<int>();
        List<int> meshNormars = new List<int>();
        List<int> meshUvs = new List<int>();
        Dictionary<int, List<int>> verticsDic = new Dictionary<int, List<int>>();
        Dictionary<int, List<int>> triangles = new Dictionary<int, List<int>>();

        foreach (Face face in faces)
        {
            for (int i = 0; i < 3; i++)
            {
                bool newpoint = true;
                int pointIndex = face.vertices[i];
                if (verticsDic.ContainsKey(pointIndex))
                {
                    Vector3 currentnormal = normals[face.normals[i]];
                    Vector3 currentuv = uvs[face.uvs[i]];
                    foreach (int dicindex in verticsDic.GetObject(pointIndex))
                    {
                        Vector3 orguv = uvs[meshUvs[dicindex]];
                        Vector3 orgnormal = normals[meshNormars[dicindex]];
                        if (currentuv == orguv && currentnormal == orgnormal)
                        {
                            newpoint = false;
                            pointIndex = dicindex;
                            break;
                        }

                    }
                }
                if (newpoint)
                {
                    meshVertics.Add(face.vertices[i]);
                    triangles.GetObject(face.usemtl).Add(meshVertics.Count - 1);
                    meshNormars.Add(face.normals[i]);
                    meshUvs.Add(face.uvs[i]);
                    if (face.vertices[i] != meshVertics.Count - 1)
                    {
                        verticsDic.GetObject(face.vertices[i]).Add(meshVertics.Count - 1);
                    }
                }
                else
                {
                    triangles.GetObject(face.usemtl).Add(pointIndex);
                }
            }
        }
        var inVertices = meshVertics.Select(i => vertices[i]).ToList();
        meshVertics.Clear();
        var inNormals = meshNormars.Select(i => normals[i]).ToList();
        meshNormars.Clear();
        var inUvs = meshUvs.Select(i => uvs[i]).ToList();
        meshUvs.Clear();
        
        foreach (KeyValuePair<int, List<int>> pair in verticsDic)
        {
            pair.Value.Clear();
        }
        verticsDic.Clear();
      
        Loom.QueueOnMainThread(() =>
        {
            mesh.SetVertices(inVertices);

            mesh.subMeshCount = triangles.Keys.Count;
            foreach (KeyValuePair<int, List<int>> trianglekey in triangles)
            {
                mesh.SetTriangles(trianglekey.Value, trianglekey.Key);
                trianglekey.Value.Clear();
            }
            mesh.SetNormals(inNormals);
            mesh.SetUVs(0, inUvs);
            //mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            inVertices.Clear();
            inNormals.Clear();
            inUvs.Clear();
            triangles.Clear();
           
            isMeshDone = true;
        });
        Application.logMessageReceivedThreaded -= ApplicationOnLogMessageReceivedThreaded;
    }

    internal void FinishObj()
    {
    
        isDone = true;
    }
}