﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using CommonTool;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class ObjData
{
    public string name;
    public string fullname;
    public bool isDone { get; private set; }
    public string mtllib;
    public GameObject gameobject;

    private List<ObjMeshData> _meshDatas = new List<ObjMeshData>();
    private GameObject _prefab;
    private Stopwatch stopwatch;


    private static List<ObjData> _loadedObjs = new List<ObjData>();

    private int vertstartindex = 0;
    private int normarstartindex = 0;
    private int uvstartindex = 0;

    public ObjData(string name)
    {
        this.name = name;
        stopwatch = new Stopwatch();
        stopwatch.Start();
    }

    public IEnumerator WaitAnyLoad()
    {
        ObjData towait = null;
        lock (_loadedObjs)
        {
            towait = _loadedObjs.Find(o => o.fullname == fullname);
            Debug.Log(towait);
            if (towait == null)
            {
                _loadedObjs.Add(this);
            }
        }
        if (towait != null)
        {
            if (!towait.isDone)
            {
                Debug.Log("等代上一个" + name + "完成");
            }
            else
            {
                Debug.Log("已发现加载的" + name + "");
            }
            while (!towait.isDone)
            {
                yield return null;
            }
            _prefab = towait._prefab;
            InstanceGameObject();
        }
        yield break;
    }

    public IEnumerator LoadObj(byte[] wwwBytes)
    {
        _prefab = new GameObject(name);
        Debug.Log("创建空物体：" + name);

        Debug.Log("开始读入模型数据：" + name + LogTime());
        using (MemoryStream stream = new MemoryStream(wwwBytes))
        {
            Encoding encoding = Encoding.Default;

            using (StreamReader reader = new StreamReader(stream, encoding))
            {
                yield return ReadObjReader(reader, InstanceMeshData);
            }
        }
        Debug.Log("模型数据读入完成：" + name + LogTime());

        yield break;
    }

    private void InstanceMeshData(ObjMeshData meshdata)
    {
        GameObject child = new GameObject(meshdata.meshname);
        child.AddComponent<MeshFilter>();
        child.AddComponent<MeshRenderer>();
        child.transform.SetParent(_prefab.transform, false);
        ObjLoaderBehaviour.GetInstance().StartCoroutine(InstanceMeshDataThread(child, meshdata));

    }

    private IEnumerator InstanceMeshDataThread(GameObject child, ObjMeshData meshdata)
    {
        while (meshdata.GetMesh() == null)
        {
            yield return null;
        }
        Mesh mesh = meshdata.GetMesh();
        MeshFilter meshFilter = child.GetComponent<MeshFilter>() ?? child.AddComponent<MeshFilter>();
        meshFilter.sharedMesh = mesh;

    }

    public IEnumerator LaodMtllib(byte[] wwwBytes)
    {
        Debug.Log("开始加载obj 材质数据：" + name + LogTime());
        using (MemoryStream stream = new MemoryStream(wwwBytes))
        {
            Encoding encoding = Encoding.Default;

            using (StreamReader reader = new StreamReader(stream, encoding))
            {

            }
        }
        yield break;
    }

    IEnumerator ReadObjReader(StreamReader reader, Action<ObjMeshData> objMeshAction)
    {

        while (reader.BaseStream.Position < reader.BaseStream.Length)
        {
            string line = reader.ReadLine();
            if (line != null)
            {
                line = string.Copy(line);
            }

            if (!string.IsNullOrEmpty(line))
            {
                if (ReadObjLine(line) || reader.BaseStream.Position == reader.BaseStream.Length)
                {
                    if (_meshDatas.LastOrDefault() != null && objMeshAction != null)
                    {
                        ObjMeshData meshData = _meshDatas.Last();
                        meshData.FinishObj();
                        Debug.Log(string.Format("{0}具有{1}个顶点,{2}个法线,{3}个uv,{4}个面", meshData.meshname, meshData.vertices.Count, meshData.normals.Count, meshData.uvs.Count, meshData.faces.Count) + LogTime());
                        yield return new WaitForFrame();
                        objMeshAction(meshData);
                    }
                    yield return new WaitForFrame();
                }
            }
        }
        yield break;
    }

    bool ReadObjLine(string ln)
    {
        ObjMeshData currentMeshData = _meshDatas.LastOrDefault();
        if (!string.IsNullOrEmpty(ln))
        {
            if (ln[0] != '#')
            {
                string l = ln.Trim().Replace("  ", " ");
                string[] cmps = l.Split(' ');
                string data = l.Remove(0, l.IndexOf(' ') + 1);

                if (cmps[0] == "mtllib")
                {
                    //load cache
                    mtllib = data;

                }
                else if ((cmps[0] == "g" || cmps[0] == "o"))
                {
                    currentMeshData.meshname = data;
                }
                else if (cmps[0] == "usemtl")
                {
                    currentMeshData.usemtl.Add(data);
                }
                else if (cmps[0] == "v")
                {
                    //VERTEX
                    Vector3 ver = OBJLoader.ParseVectorFromCMPS(cmps);
                    ver.x = -ver.x;
                    currentMeshData.vertices.Add(ver);
                }
                else if (cmps[0] == "vn")
                {
                    //VERTEX NORMAL
                    currentMeshData.normals.Add(OBJLoader.ParseVectorFromCMPS(cmps));
                }
                else if (cmps[0] == "vt")
                {
                    //VERTEX UV
                    currentMeshData.uvs.Add(OBJLoader.ParseVectorFromCMPS(cmps));
                }
                else if (cmps[0] == "f")
                {
                    List<int[]> face = new List<int[]>();
                    for (int i = 1; i < cmps.Length; i++)
                    {
                        string felement = cmps[i];
                        int vertexIndex = -1;
                        int normalIndex = -1;
                        int uvIndex = -1;
                        if (felement.Contains("//"))
                        {
                            //doubleslash, no UVS.
                            string[] elementComps = felement.Split('/');
                            vertexIndex = int.Parse(elementComps[0]) - 1;
                            normalIndex = int.Parse(elementComps[2]) - 1;
                        }
                        else if (felement.Count(x => x == '/') == 2)
                        {
                            //contains everything
                            string[] elementComps = felement.Split('/');
                            vertexIndex = int.Parse(elementComps[0]) - 1;
                            uvIndex = int.Parse(elementComps[1]) - 1;
                            normalIndex = int.Parse(elementComps[2]) - 1;
                        }
                        else if (!felement.Contains("/"))
                        {
                            //just vertex inedx
                            vertexIndex = int.Parse(felement) - 1;
                        }
                        else
                        {
                            //vertex and uv
                            string[] elementComps = felement.Split('/');
                            vertexIndex = int.Parse(elementComps[0]) - 1;
                            uvIndex = int.Parse(elementComps[1]) - 1;
                        }
                        int[] index = new int[]
                        {
                            vertexIndex-vertstartindex,
                            normalIndex-normarstartindex,
                            uvIndex-uvstartindex
                        };
                        face.Add(index);
                    }
                    face.Reverse();
                    currentMeshData.AddFace(face);
                }
            }
            else if (ln.Contains("# object"))
            {
                if (currentMeshData != null)
                {
                    vertstartindex += currentMeshData.vertices.Count;
                    normarstartindex += currentMeshData.normals.Count;
                    uvstartindex += currentMeshData.uvs.Count;
                }
                _meshDatas.Add(new ObjMeshData());
            }
            else if (ln.Contains("faces"))
            {
                return true;
            }

        }
        return false;
    }

    public GameObject InstanceGameObject()
    {
        _prefab.SetActive(false);
        gameobject = Object.Instantiate(_prefab);
        gameobject.SetActive(true);
        Debug.Log("实例化物体:" + gameobject.name + "。" + LogTime());
        isDone = true;
        return gameobject;
    }
    public string LogTime()
    {
        return "用时 " + stopwatch.Elapsed.TotalSeconds.ToString("f3");
    }
}