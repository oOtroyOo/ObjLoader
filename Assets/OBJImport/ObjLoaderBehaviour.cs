﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommonTool;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class ObjLoaderBehaviour : InstanceMonoBehavior<ObjLoaderBehaviour>
{
    public string baseUrl;
    public void Load(string name)
    {
        string url = Path.Combine(baseUrl, name);
        if (!Path.HasExtension(url))
        {
            url += ".obj";
        }
        url = url.Replace("\\", "/");
        Debug.Log("准备加载" + url);
        StartCoroutine(ILoad(name, o => Debug.Log(o.name + "加载完毕")));
    }


    public void SetUrl(string s)
    {
        baseUrl = s;
    }

    IEnumerator ILoad(string name, Action<GameObject> gameobjectAction)
    {
        ObjData objData = new ObjData(name);

        string urlObj = Path.Combine(baseUrl, name);
        if (!Path.HasExtension(urlObj))
        {
            urlObj += ".obj";
        }
        urlObj = urlObj.Replace("\\", "/");
        objData.fullname = urlObj;

        yield return objData.WaitAnyLoad();
        if (objData.gameobject != null)
        {
            if (gameobjectAction != null)
            {
                gameobjectAction(objData.gameobject);
            }
            yield break;
        }
      
        
        WWW www = new WWW(urlObj);
        yield return www;


        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.LogError(www.error);
        }
        else
        {
            Debug.Log("下载完毕" + objData.LogTime());
            yield return objData.LoadObj(www.bytes);
        }
        www.Dispose();

        GC.Collect();
        objData.InstanceGameObject();
        if (objData.gameobject != null)
        {
            if (gameobjectAction != null)
            {
                gameobjectAction(objData.gameobject);
            }
            yield break;
        }

        yield break;
    }
}