﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class NewBehaviourScript : MonoBehaviour
{
    public InputField UrlField;
    public InputField[] names;
    public Button[] buttons;
    public virtual void Awake()
    {
        OnUrlChange(UrlField.text);
        UrlField.onValueChanged.AddListener(OnUrlChange);
        var inputs = GetComponentsInChildren<InputField>().ToList();
        inputs.Remove(UrlField);
        names = inputs.ToArray();
        buttons = GetComponentsInChildren<Button>();
        for (var i = 0; i < buttons.Length; i++)
        {
            Button button = buttons[i];
            int j = i;
            button.onClick.AddListener(() => Load(j));
        }
    }

    private void Load(int i)
    {
        ObjLoaderBehaviour.GetInstance().Load(names[i].text);
    }

    public virtual void Start()
    {
        ObjLoaderBehaviour.GetInstance();
    }

    public virtual void Update()
    {

    }

    public virtual void OnEnable()
    {

    }

    public virtual void OnDisable()
    {

    }

    public virtual void FixedUpdate()
    {

    }

    public virtual void LateUpdate()
    {

    }

    public void OnUrlChange(string s)
    {
        ObjLoaderBehaviour.GetInstance().SetUrl(s);
    }
}